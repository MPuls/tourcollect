package de.mipnet.tourcollect;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import de.mipnet.tourcollect.currenttour.StoreOnLocationChange;
import de.mipnet.tourcollect.datamodel.SpeedLimitsPojo;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

/**
 * Lifecycle of a (unbounded) Service: Call to startService() --> onCreate() --> onStartCommand() --> Service running (the Service is stopped by itself or a client --> onDestroy() --> Service shut down
 */

public class LocationService extends Service implements LocationListener {
    final String TAG = this.getClass().getSimpleName();
    private Context applicationContext;
    public static boolean isRunning = false;
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // in meter
    private static final long MIN_TIME_BW_UPDATES = 500; // in milliseconds
    private StoreOnLocationChange storeOnLocationChange;

    @Override
    public IBinder onBind(Intent arg0) {
        // We don't provide binding, so return null
        return null;
    }

    /**
    * The system invokes the method onCreate() to perform one-time setup procedures when the service is initially created (before it calls either onStartCommand() or onBind()).
    * If the service is already running, this method is not called.
    */
    public void onCreate() {
        this.applicationContext = getApplicationContext();
    }

    // TODO Remove Suppression and fix the issue
    @SuppressLint("MissingPermission")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(this, getString(R.string.app_name) + ": " + getString(R.string.toast_location_service_start), Toast.LENGTH_SHORT).show();
        isRunning = true;

        // Declaring a Location Manager
        LocationManager locationManager;
        locationManager = (LocationManager) applicationContext.getSystemService(Context.LOCATION_SERVICE);

        // TODO Request permission here

        /*
        LocationListener: Used for receiving notifications from the LocationManager when the location has changed.
        The methods onLocationChanged(), onProviderDisabled(), onProviderEnabled() and onStatusChanged() are called if the LocationListener has been registered with the location manager service using the requestLocationUpdates(String, long, float, LocationListener) method.
        */

        if (locationManager != null) {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);
            Log.i(TAG, "onStartCommand:  Location Service started");
        }

        var tourRepository = TourRepository.getInstance(applicationContext);
        var speedLimits = new SpeedLimitsPojo(applicationContext);
        storeOnLocationChange = new StoreOnLocationChange(tourRepository,speedLimits.getSpeedLimitMoving());
        return super.onStartCommand(intent, flags, startId);
    }

    /**
     * The system invokes the method onDestroy() when the service is no longer used and is being destroyed.
     * Your service should implement this to clean up any resources such as threads, registered listeners, or receivers.
     * This is the last call that the service receives.
     */
    @Override
    public void onDestroy() {
        Toast.makeText(this, getString(R.string.app_name) + ": " + getString(R.string.toast_location_service_stop), Toast.LENGTH_SHORT).show();
        super.onDestroy();
        isRunning = false;
        Log.i(TAG, "onDestroy: Location Service stopped");
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        // Provide Location Properties to the outside
        Log.d(TAG, "onLocationChanged: location: " + location);
        storeOnLocationChange.onLocationChange(location);
    }

    /**
     * Called when the provider is disabled by the user. If requestLocationUpdates is called on an already disabled provider, this method is called immediately.
     * @param provider String: the name of the location provider associated with this update.
     */
    @Override
    public void onProviderDisabled(@NonNull String provider) {
        Log.e(TAG, "onProviderDisabled: " + provider + " seems not to be enabled");
        Toast.makeText(this, getString(R.string.app_name) + ": " + provider + " " + getString(R.string.toast_gps_disabled), Toast.LENGTH_SHORT).show();
    }

    /**
     * Called when the provider is enabled by the user.
     * @param provider String: the name of the location provider associated with this update.
     */
    @Override
    public void onProviderEnabled(@NonNull String provider) {
        Log.i(TAG, "onProviderEnabled: " + provider + " is enabled");
        Toast.makeText(this, getString(R.string.app_name) + ": " + provider + " " + getString(R.string.toast_gps_enabled), Toast.LENGTH_SHORT).show();
    }
}
