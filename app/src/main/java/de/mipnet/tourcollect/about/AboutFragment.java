package de.mipnet.tourcollect.about;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.mipnet.tourcollect.BuildConfig;
import de.mipnet.tourcollect.R;

public class AboutFragment extends Fragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvVersion = view.findViewById(R.id.about_version);
        tvVersion.setText(getString(R.string.about_version, BuildConfig.VERSION_NAME));

        TextView tvUsedLibs = view.findViewById(R.id.about_libs);
        String htmlAsString = getString(R.string.about_libs);
        tvUsedLibs.setText(Html.fromHtml(htmlAsString));
    }
}
