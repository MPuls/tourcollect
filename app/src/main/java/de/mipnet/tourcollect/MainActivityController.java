package de.mipnet.tourcollect;

import android.util.Log;


interface FinishAllActivities {
}

public class MainActivityController {
    final String TAG = this.getClass().getSimpleName();
    private MainActivity mainActivity;
    private static MainActivityController instance;

    private MainActivityController() {}

    public static MainActivityController getInstance() {
        if(instance == null) instance = new MainActivityController();
        return instance;
    }

    void setMainActivity(MainActivity mainActivity) {
        Log.d(TAG, "setMainActivity: called");
        this.mainActivity = mainActivity;
    }

    public void triggerFinishAllActivities() {
        Log.d(TAG, "triggerRestart: called with mainActiviy (" + mainActivity + ")");
        if(mainActivity != null) mainActivity.finishAllActivities();
    }
}
