package de.mipnet.tourcollect;

import android.content.res.Resources;
import android.util.Log;

import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.mipnet.tourcollect.StaticVariables.DAY;
import static de.mipnet.tourcollect.StaticVariables.HOUR;
import static de.mipnet.tourcollect.StaticVariables.MINUTE;
import static de.mipnet.tourcollect.StaticVariables.MONTH;
import static de.mipnet.tourcollect.StaticVariables.SECOND;
import static de.mipnet.tourcollect.StaticVariables.WEEK;
import static de.mipnet.tourcollect.StaticVariables.YEAR;

/**
 * Object for converting observables (physical meaning) into strings with unit specific values.
 */

public class FormatHelper {

    //private static final String TAG = getClass().getSimpleName();
    private static final String TAG = "FormatHelper";
    @SuppressWarnings("FieldMayBeFinal")
    private static Locale LOCAL = Locale.getDefault();

    public static Number localStringToNumber(String string) throws ParseException {
        NumberFormat numberFormat = NumberFormat.getInstance(LOCAL);
        return numberFormat.parse(string);
    }

    public static String formatSpeed(double speed) {
        return String.format(LOCAL,"%.0f", mPerSToKmPerH(speed));
    }

    public static String formatAverageSpeed(double speed) {
        return String.format(LOCAL,"%.1f", mPerMsToKmPerH(speed));
    }

    public static String formatAverageSpeedSimple(double speed) {
        return String.format(LOCAL,"%.0f", mPerMsToKmPerH(speed));
    }

    public static String formatDistance(double distance) {
        return String.format(LOCAL, "%.1f", mToKm(distance));
    }

    public static String formatDistanceSimple(double distance) {
        return String.format(LOCAL, "%.0f", mToKm(distance));
    }

    public static Map<String, String> formatDuration(long duration) {
        Map<String, String> formatDuration = new HashMap<>();
        formatDuration.put(SECOND, String.format(LOCAL, "%02d",TimeUnit.MILLISECONDS.toSeconds(duration) % TimeUnit.MINUTES.toSeconds(1)));
        formatDuration.put(MINUTE, String.format(LOCAL, "%02d",TimeUnit.MILLISECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1)));
        formatDuration.put(HOUR, String.format(LOCAL, "%d", TimeUnit.MILLISECONDS.toHours(duration)));
        return formatDuration;
    }

    public static String formatDurationHm(long duration) {
        long hours = TimeUnit.MILLISECONDS.toHours(duration);
        long minutes = TimeUnit.MILLISECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1);

        return String.format(LOCAL, "%02d", hours) + ":" + String.format(LOCAL, "%02d", minutes);
    }

    public static String formatDurationSec(long duration) {
        return String.format(LOCAL, "%d", duration/1000);
    }

    public static String formatTimestampToTimePeriodUnit(Timestamp timestamp, String timePeriodUnit, Resources resources) {
        String result;
        switch (timePeriodUnit) {
            case DAY:
                if(isToday(timestamp)) {
                    result = resources.getString(R.string.today);
                } else if (isYesterday(timestamp)) {
                    result = resources.getString(R.string.yesterday);
                } else {
                    result = new SimpleDateFormat("E, dd. MMM", LOCAL).format(timestamp);
                }
                break;
            case WEEK:
                if(isThisWeek(timestamp)) {
                    result = resources.getString(R.string.this_week);
                } else if(isLastWeek(timestamp)) {
                    result = resources.getString(R.string.last_week);
                } else {
                    result = resources.getString(R.string.CW) + " " + new SimpleDateFormat("w", LOCAL).format(timestamp);
                }
                break;
            case MONTH:
                result = new SimpleDateFormat("MMMM", LOCAL).format(timestamp);
                break;
            case YEAR:
                result = new SimpleDateFormat("y", LOCAL).format(timestamp);
                break;
            default:
                result = "No known time period unit.";
                break;
        }
        return result;
    }

    public static String formatTimestampToTimePeriodUnitChart(Timestamp timestamp, String timePeriodUnit, Resources resources) {
        return switch (timePeriodUnit) {
            case DAY -> new SimpleDateFormat("E dd.MM.", LOCAL).format(timestamp);
            case WEEK ->
                    resources.getString(R.string.CW) + " " + new SimpleDateFormat("w", LOCAL).format(timestamp);
            case MONTH -> new SimpleDateFormat("MMM", LOCAL).format(timestamp);
            case YEAR -> new SimpleDateFormat("y", LOCAL).format(timestamp);
            default -> "No known time period unit.";
        };
    }

    public static String formatTimestampToDay(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("E, dd. MMM", LOCAL);
        return dateFormat.format(timestamp);
    }

    public static String formatTimestampToTime(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", LOCAL);
        return dateFormat.format(timestamp);
    }

    public static String formatTimestampToExportFormat(Timestamp timestamp) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss", LOCAL);
        return dateFormat.format(timestamp);
    }

    public static long getMillisFromHm(String string) throws ParseException {
        if (!isHmFormat(string)) {
            throw new ParseException(string + "does not match format HH:mm.",0);
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", LOCAL);
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = simpleDateFormat.parse("1970-01-01 " + string);
        assert date != null;
        return date.getTime();
    }

    private static double mPerSToKmPerH(double speed) {
        return speed/1000 * 60 * 60;
    }

    private static double mPerMsToKmPerH(double speed) {
        return mPerSToKmPerH(speed) * 1000;
    }

    private static double mToKm(double distance) {
         return distance / 1000;
    }

    private static boolean isHmFormat(String string) {
        Pattern durationFormat = Pattern.compile("(\\d+):(\\d+)");
        Matcher m = durationFormat.matcher(string);
        if (m.matches()) {
            return true;
        } else {
            Log.d(TAG, "isHmFormat: " + string + " does not match format HH:mm.");
            return false;
        }
    }

    private static boolean isToday(Timestamp timestamp) {
        Calendar now = Calendar.getInstance();
        Calendar timeToCheck = Calendar.getInstance();
        timeToCheck.setTimeInMillis(timestamp.getTime());
        return (timeToCheck.get(Calendar.YEAR) == now.get(Calendar.YEAR) && timeToCheck.get(Calendar.DAY_OF_YEAR) == now.get(Calendar.DAY_OF_YEAR));
    }

    private static boolean isYesterday(Timestamp timestamp) {
        Calendar yesterday = Calendar.getInstance();
        yesterday.add(Calendar.DAY_OF_YEAR, -1);
        Calendar timeToCheck = Calendar.getInstance();
        timeToCheck.setTimeInMillis(timestamp.getTime());
        return (timeToCheck.get(Calendar.YEAR) == yesterday.get(Calendar.YEAR) && timeToCheck.get(Calendar.DAY_OF_YEAR) == yesterday.get(Calendar.DAY_OF_YEAR));
    }

    private static boolean isThisWeek(Timestamp timestamp) {
        Calendar now = Calendar.getInstance();
        Calendar timeToCheck = Calendar.getInstance();
        timeToCheck.setTimeInMillis(timestamp.getTime());
        return (timeToCheck.get(Calendar.YEAR) == now.get(Calendar.YEAR) && timeToCheck.get(Calendar.WEEK_OF_YEAR) == now.get(Calendar.WEEK_OF_YEAR));
    }

    private static boolean isLastWeek(Timestamp timestamp) {
        Calendar lastWeek = Calendar.getInstance();
        lastWeek.add(Calendar.WEEK_OF_YEAR, -1);
        Calendar timeToCheck = Calendar.getInstance();
        timeToCheck.setTimeInMillis(timestamp.getTime());
        return (timeToCheck.get(Calendar.YEAR) == lastWeek.get(Calendar.YEAR) && timeToCheck.get(Calendar.WEEK_OF_YEAR) == lastWeek.get(Calendar.WEEK_OF_YEAR));
    }
}
