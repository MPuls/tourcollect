package de.mipnet.tourcollect.helper;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.InputStream;
import java.net.URLConnection;

import androidx.annotation.Nullable;

import static de.mipnet.tourcollect.StaticVariables.DB_MIME_TYPE;

public class FileHelper {
    private final static String TAG = "FileHelper";

    @Nullable
    public static String getMimeType(Uri uri, Context context) {

        String mimeType = null;
        String fileName = getFileName(uri, context);

        try {
            if (uri.getScheme().equals(ContentResolver.SCHEME_CONTENT)) {
                ContentResolver cr = context.getContentResolver();
                mimeType = cr.getType(uri);
                Log.i(TAG, "getMimeType: via ContentResolver: [" + mimeType + "]");
            }
            if (mimeType == null || !mimeType.equals(DB_MIME_TYPE)) {
                try {
                    ParcelFileDescriptor parcelFileDescriptor = context.getContentResolver().openFileDescriptor(uri, "r");
                    FileDescriptor fileDescriptor = parcelFileDescriptor.getFileDescriptor();
                    InputStream is = new FileInputStream(fileDescriptor);
                    mimeType = URLConnection.guessContentTypeFromStream(is);
                    parcelFileDescriptor.close();
                    Log.i(TAG, "getMimeType: via URLConnection.guessContentTypeFromStream: [" + mimeType + "]");
                } catch (NullPointerException e) {
                    return null;
                }
            }
            if (mimeType == null || !mimeType.equals(DB_MIME_TYPE)) {
                mimeType= URLConnection.guessContentTypeFromName(getFileName(uri, context));
                Log.i(TAG, "getMimeType: via URLConnection.guessContentTypeFromName(): [" + mimeType + "]");
            }
            if (mimeType == null || !mimeType.equals(DB_MIME_TYPE)) {
                String[] fileNameSplit = fileName.split("\\.");
                String fileExtension = fileNameSplit[fileNameSplit.length - 1];
                if (fileExtension.equals("db")) mimeType = DB_MIME_TYPE;
                Log.i(TAG, "getMimeType: via fileExtension.equals(\"db\"): [" + mimeType + "]");
            }
        } catch (Exception e) {
            Log.d(TAG, "getMimeType: exception: " + e);
        }
        return mimeType;
    }

    public static String getFileName(Uri uri, Context context) {
        Cursor returnCursor =
                context.getContentResolver().query(uri, null, null, null, null);
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        returnCursor.moveToFirst();
        String fileName =  returnCursor.getString(nameIndex);
        returnCursor.close();
        return fileName;
    }
}
