package de.mipnet.tourcollect.lasttours.view;


import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.lasttours.view.edittour.EditTourDialogFragment;
import de.mipnet.tourcollect.lasttours.viewmodel.LastToursVM;

import static de.mipnet.tourcollect.StaticVariables.BIKE;
import static de.mipnet.tourcollect.StaticVariables.CAR;
import static de.mipnet.tourcollect.StaticVariables.RUN;
import static de.mipnet.tourcollect.StaticVariables.SQL_ANY;
import static de.mipnet.tourcollect.StaticVariables.WALK;

public class LastToursFragment extends Fragment implements TourOptionClickListener {

    private final String TAG = this.getClass().getSimpleName();

    private LastToursVM lastToursVM;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.last_tours, container, false); //returns a View
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        lastToursVM = new ViewModelProvider(this).get(LastToursVM.class);

        final LastToursAdapter lastToursAdapter = new LastToursAdapter(this);

        RecyclerView recyclerView = view.findViewById(R.id.last_tours_recycler_view);
        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(LayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        lastToursVM.tourList.observe(requireActivity(), lastToursAdapter::submitList);

        recyclerView.setAdapter(lastToursAdapter);
        lastToursVM.setFilter(SQL_ANY);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.last_tours_toolbar, menu);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onTourOptionClick(Tour tour, ImageView btnOptions) {
        Context context = getContext();
        PopupMenu popupMenu = new PopupMenu(context, btnOptions);
        popupMenu.inflate(R.menu.tour_list_row_options_menu);
        popupMenu.setOnMenuItemClickListener(item -> {
            switch (item.getItemId()) {
                case R.id.tourListRow_optionsMenu_delete:
                    if (!tour.isRunningStatus()) {
                        Log.d(TAG, "onMenuItemClick: try to delete tour with id " + tour.getId());
                        lastToursVM.deleteTour(tour);
                        Toast.makeText(context, getString(R.string.app_name) + ": " + getString(R.string.toast_tour_deleted), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getString(R.string.app_name) + ": " + getString(R.string.toast_tour_not_deleted), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case R.id.tourListRow_optionsMenu_edit:
                    EditTourDialogFragment editTourDialogFragment = new EditTourDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putLong("TOUR_ID", tour.getId());
                    editTourDialogFragment.setArguments(bundle);
                    editTourDialogFragment.show(requireActivity().getSupportFragmentManager(), "editTourDialog");
                    break;
            }
            return false;
        });
        popupMenu.show();
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.filter_walk -> lastToursVM.setFilter(WALK);
            case R.id.filter_run -> lastToursVM.setFilter(RUN);
            case R.id.filter_bike -> lastToursVM.setFilter(BIKE);
            case R.id.filter_car -> lastToursVM.setFilter(CAR);
            default -> lastToursVM.setFilter(SQL_ANY);
        }
        return super.onOptionsItemSelected(item);
    }
}
