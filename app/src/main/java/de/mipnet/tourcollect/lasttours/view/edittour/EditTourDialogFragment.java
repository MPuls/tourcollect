package de.mipnet.tourcollect.lasttours.view.edittour;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.lasttours.viewmodel.LastToursVM;

import static de.mipnet.tourcollect.StaticVariables.MOVINGTYPES;

public class EditTourDialogFragment extends DialogFragment implements AdapterView.OnItemSelectedListener {

    private final String TAG = this.getClass().getSimpleName();

    private LastToursVM lastToursVM;
    boolean isCorrectFormatting = true;

    TextView tvDate, tvTimes, btnCancel, btnSave;
    EditText etDistance, etDurationTour, etDurationMoving;
    Spinner spinnerMovingType;

    long tourId;
    Tour tour;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.dialog_edit_tour, container, false); //returns a View
    }

    @Override
    public void setArguments(Bundle bundle) {
        if (bundle != null) {
            tourId = bundle.getLong("TOUR_ID");
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        tvDate = view.findViewById(R.id.dialogEditTour_date_tv);
        tvTimes = view.findViewById(R.id.dialogEditTour_times_tv);
        btnCancel = view.findViewById(R.id.dialogEditTour_cancle_tv);
        btnSave = view.findViewById(R.id.dialogEditTour_save_tv);
        spinnerMovingType = view.findViewById(R.id.dialogEditTour_movingType_spinner);
        etDistance = view.findViewById(R.id.dialogEditTour_distance_value_et);
        etDurationMoving = view.findViewById(R.id.dialogEditTour_durationMoving_value_et);
        etDurationTour = view.findViewById(R.id.dialogEditTour_durationTour_value_et);

        lastToursVM = new ViewModelProvider(this).get(LastToursVM.class);
        tour = lastToursVM.getTourWithId(tourId);

        populateDialogWithDbValues();

        // changes in moving type spinner are saved to the tour object
        spinnerMovingType.setOnItemSelectedListener(this);

        btnCancel.setOnClickListener(v -> dismiss());
        btnSave.setOnClickListener(v -> {
            double distance = tour.getDistance();
            long durationTour = tour.getDurationTour();
            long durationMoving = tour.getDurationMoving();
            isCorrectFormatting = true;
            String distanceString = etDistance.getText().toString();
            String durationTourString = etDurationTour.getText().toString();
            String durationMovingString = etDurationMoving.getText().toString();

            // Set text color for the editText fields to the primary text color in order to be able to change them repeatably, if the format of the inserted text is wrong.
            etDistance.setTextColor(ContextCompat.getColor(requireContext(),R.color.black));
            etDurationTour.setTextColor(ContextCompat.getColor(requireContext(),R.color.black));
            etDurationMoving.setTextColor(ContextCompat.getColor(requireContext(),R.color.black));

            //
            Log.i(TAG, "onViewCreated: distance " + distance);
            try {
                distance = FormatHelper.localStringToNumber(distanceString).doubleValue()*1000;
                durationTour = durationStringToLong(durationTourString, etDurationTour);
                durationMoving = durationStringToLong(durationMovingString, etDurationMoving);
            } catch (Exception e) {
                Log.d(TAG, "btnSave.onClick: " +  e);
                isCorrectFormatting = false;
                Toast.makeText(requireContext(), getString(R.string.incorrect_value_format), Toast.LENGTH_LONG).show();
            }

            if (isCorrectFormatting) {
                tour.setDistance(distance);
                tour.setDurationTour(durationTour);
                tour.setDurationMoving(durationMoving);

                lastToursVM.updateTour(tour);
                dismiss();
            }
        });

        etDistance.addTextChangedListener(new EditTourDistanceTextChangeWatcher(etDistance));
    }

    //
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, "onItemSelected: " + parent.getSelectedItemPosition());
        tour.setMovingType(MOVINGTYPES[parent.getSelectedItemPosition()]);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void populateDialogWithDbValues() {
        tvDate.setText(FormatHelper.formatTimestampToDay(tour.getCreated()));
        tvTimes.setText(getString(
                R.string.times_interval_value_format,
                FormatHelper.formatTimestampToTime(tour.getCreated()),
                FormatHelper.formatTimestampToTime(tour.getLastUpdate())
        ));
        etDistance.setText(FormatHelper.formatDistance(tour.getDistance()));
        etDurationTour.setText(FormatHelper.formatDurationHm(tour.getDurationTour()));
        etDurationMoving.setText(FormatHelper.formatDurationHm(tour.getDurationMoving()));

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(requireContext(), R.array.movingTypes, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerMovingType.setAdapter(adapter);
        spinnerMovingType.setSelection(Arrays.asList(MOVINGTYPES).indexOf(tour.getMovingType()));
    }

    // duration (String) --> duration (long) and handling textEdit field if the string has not the right format
    private long durationStringToLong(String durationString, EditText editText) throws ParseException {
        long duration = 0;
        try {
            duration = FormatHelper.getMillisFromHm(durationString);
        } catch(ParseException pe) {
            editText.setTextColor(ContextCompat.getColor(requireContext(), R.color.red));
            throw pe;
        } catch (Exception e){
            Log.d(TAG, "durationStringToLong: " + e);
        }

        return duration;
    }
}
