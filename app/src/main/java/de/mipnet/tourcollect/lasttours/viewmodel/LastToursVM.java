package de.mipnet.tourcollect.lasttours.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;


public class LastToursVM extends AndroidViewModel {
    private final TourRepository tourRepository;
    private final MutableLiveData<String> filter = new MutableLiveData<>();
    public final LiveData<PagedList<Tour>> tourList;

    public LastToursVM(@NonNull Application application) {
        super(application);
        tourRepository = TourRepository.getInstance(application);
        PagedList.Config pagedListConfig = new PagedList.Config.Builder()
                .setPageSize(10)
                .build();
        
        tourList = Transformations.switchMap(filter,
                (movingType) -> {
                  return new LivePagedListBuilder(tourRepository.getTours(movingType), pagedListConfig).build();
                });
    }

    public Tour getTourWithId(long id) {
        return tourRepository.getTourWithId(id);
    }

    public void deleteTour(Tour tour) {
        tourRepository.deleteTour(tour);
    }

    public void updateTour(Tour tour) {
        tourRepository.updateTour(tour);
    }

    public void setFilter(String movingType) {
        filter.setValue(movingType);
    }
}
