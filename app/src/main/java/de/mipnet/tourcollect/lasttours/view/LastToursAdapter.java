package de.mipnet.tourcollect.lasttours.view;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.Tour;

import static de.mipnet.tourcollect.StaticVariables.BIKE;
import static de.mipnet.tourcollect.StaticVariables.CAR;
import static de.mipnet.tourcollect.StaticVariables.HOUR;
import static de.mipnet.tourcollect.StaticVariables.MINUTE;
import static de.mipnet.tourcollect.StaticVariables.RUN;

class LastToursAdapter extends PagedListAdapter<Tour,LastToursAdapter.RowViewHolder> {

    // private final String TAG = this.getClass().getSimpleName();
    private final TourOptionClickListener tourOptionClickListener;

    LastToursAdapter(TourOptionClickListener tourOptionClickListener) {
        super(DIFF_CALLBACK);
        this.tourOptionClickListener = tourOptionClickListener;
    }

    @NonNull
    @Override
    public RowViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_list_row, parent,false);
        return new RowViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(@NonNull RowViewHolder rowViewHolder, int position) {
        Tour tour = getItem(position);
        if (tour != null) {
            rowViewHolder.bindTo(Objects.requireNonNull(getItem(position)));
        }
    }

    private static final DiffUtil.ItemCallback<Tour> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<>() {
                @Override
                public boolean areItemsTheSame(Tour oldItem, Tour newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(Tour oldItem, Tour newItem) {
                    return (oldItem.getMovingType().equals(newItem.getMovingType()) &&
                            oldItem.getDistance() == newItem.getDistance() &&
                            oldItem.getDurationTour() == newItem.getDurationTour() &&
                            oldItem.getDurationMoving() == newItem.getDurationMoving());
                }
            };

    class RowViewHolder extends RecyclerView.ViewHolder {
        TextView tvCreatedDay, tvCreatedTime, tvLastUpdateTime, tvDistance, tvDurationTourHoursValue, tvDurationTourMinutesValue, tvDurationMovingHoursValue, tvDurationMovingMinutesValue, tvAverageSpeedMoving;
        ImageView btnOptions, imgMovingType;

        RowViewHolder(View itemView) {
            super(itemView);
            tvCreatedDay = itemView.findViewById(R.id.tourListRow_createdDay_tv);
            tvCreatedTime = itemView.findViewById(R.id.tourListRow_createdTime_tv);
            tvLastUpdateTime = itemView.findViewById(R.id.tourListRow_lastUpdateTime_tv);
            tvDistance = itemView.findViewById(R.id.tourListRow_distance_tv);
            tvDurationTourHoursValue = itemView.findViewById(R.id.tourListRow_durationTourHoursValue_tv);
            tvDurationTourMinutesValue = itemView.findViewById(R.id.tourListRow_durationTourMinutesValue_tv);
            tvDurationMovingHoursValue = itemView.findViewById(R.id.tourListRow_durationMovingHoursValue_tv);
            tvDurationMovingMinutesValue = itemView.findViewById(R.id.tourListRow_durationMovingMinutesValue_tv);
            tvAverageSpeedMoving = itemView.findViewById(R.id.tourListRow_averageSpeedMoving_tv);
            btnOptions = itemView.findViewById(R.id.tourListRow_more_btn);
            imgMovingType = itemView.findViewById(R.id.tourListRow_movingType_img);
        }
        
        void bindTo(Tour tour) {
            tvCreatedDay.setText(FormatHelper.formatTimestampToDay(tour.getCreated()));
            tvCreatedTime.setText(FormatHelper.formatTimestampToTime(tour.getCreated()));
            tvLastUpdateTime.setText(FormatHelper.formatTimestampToTime(tour.getLastUpdate()));
            tvDistance.setText(FormatHelper.formatDistance(tour.getDistance()));
            tvDurationTourHoursValue.setText(FormatHelper.formatDuration(tour.getDurationTour()).get(HOUR));
            tvDurationTourMinutesValue.setText(FormatHelper.formatDuration(tour.getDurationTour()).get(MINUTE));
            tvDurationMovingHoursValue.setText(FormatHelper.formatDuration(tour.getDurationMoving()).get(HOUR));
            tvDurationMovingMinutesValue.setText(FormatHelper.formatDuration(tour.getDurationMoving()).get(MINUTE));
            tvAverageSpeedMoving.setText(FormatHelper.formatAverageSpeed(tour.getAverageSpeedMoving()));

            switch (tour.getMovingType()) {
                case RUN -> imgMovingType.setImageResource(R.drawable.ic_run);
                case BIKE -> imgMovingType.setImageResource(R.drawable.ic_bike);
                case CAR -> imgMovingType.setImageResource(R.drawable.ic_car);
                default -> imgMovingType.setImageResource(R.drawable.ic_walk);
            }

        btnOptions.setOnClickListener(v -> tourOptionClickListener.onTourOptionClick(tour, btnOptions));
        }
    }

}
