package de.mipnet.tourcollect.lasttours.view.edittour;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import java.text.DecimalFormatSymbols;

class EditTourDistanceTextChangeWatcher implements TextWatcher {
    private final EditText etDistance;
    protected EditTourDistanceTextChangeWatcher(EditText etDistance) {
        super();
        this.etDistance = etDistance;
    }
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) { }
    @Override
    public void afterTextChanged(Editable s) {
        char separator = DecimalFormatSymbols.getInstance().getDecimalSeparator();

        String sanitizedValue = s.toString();
        sanitizedValue = sanitizedValue.replaceAll(",", Character.toString(separator));
        sanitizedValue = sanitizedValue.replaceAll("\\.", Character.toString(separator));
        int firstSeparatorIndex = sanitizedValue.indexOf(separator);
        if (firstSeparatorIndex > 0 ) {
            var first = sanitizedValue.substring(0, firstSeparatorIndex + 1);
            var second =  sanitizedValue.substring(firstSeparatorIndex + 1).replaceAll(Character.toString(separator), "");
            sanitizedValue = first + second;
            if (!sanitizedValue.equals(s.toString())) {
                etDistance.setText(sanitizedValue);
            }
        }
    }
}
