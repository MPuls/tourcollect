package de.mipnet.tourcollect.lasttours.view;

import android.widget.ImageView;

import de.mipnet.tourcollect.datamodel.Tour;

interface TourOptionClickListener {
    void onTourOptionClick(Tour tour, ImageView btnOptions);
}
