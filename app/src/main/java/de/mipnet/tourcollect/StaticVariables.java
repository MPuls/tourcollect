package de.mipnet.tourcollect;

import java.util.HashMap;

public final class StaticVariables {
    public final static String BIKE = "bike";
    public final static String CAR = "car";
    public final static String WALK = "walk";
    public final static String RUN = "run";
    public final static String SQL_ANY = "%";
    public final static String[] MOVINGTYPES = {WALK, RUN, BIKE, CAR, SQL_ANY};
    public final static String MOVING_TYPE = "MOVING_TYPE";
    public final static String SECOND = "second";
    public final static String MINUTE = "minute";
    public final static String HOUR = "hour";
    public final static String DAY = "day";
    public final static String WEEK = "week";
    public final static String MONTH = "month";
    public final static String YEAR = "year";
    public static final HashMap<String, Integer> MOVING_TYPE_MAP = new HashMap<>();
    static {
        MOVING_TYPE_MAP.put(WALK, 0);
        MOVING_TYPE_MAP.put(RUN, 1);
        MOVING_TYPE_MAP.put(BIKE, 2);
        MOVING_TYPE_MAP.put(CAR, 3);
    }
    public static final HashMap<String, Integer> TIME_PERIOD_TYPE_MAP = new HashMap<>();
    static {
        TIME_PERIOD_TYPE_MAP.put(DAY, 0);
        TIME_PERIOD_TYPE_MAP.put(WEEK, 1);
        TIME_PERIOD_TYPE_MAP.put(MONTH, 2);
        TIME_PERIOD_TYPE_MAP.put(YEAR, 3);
    }
    public final static String DATABASENAME = "tour-database";
    public final static String DB_MIME_TYPE = "application/vnd.sqlite3";
}