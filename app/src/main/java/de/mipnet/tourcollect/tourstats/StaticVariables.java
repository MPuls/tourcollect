package de.mipnet.tourcollect.tourstats;

public final class StaticVariables {
    public static final String TIME_PERIOD_TYPE = "TIME_PERIOD_TYPE_ARG";
    public static final String TIME = "ARG_Timestamp_as_long";
    public static final String DISTANCE = "DISTANCE_ARG";
    public static final String DURATION = "DURATION_ARG";
    public static final String AVERAGE_SPEED = "AVERAGE_SPEED_ARG";
    public static final int CHART_MAX_ENTRIES = 10;
}
