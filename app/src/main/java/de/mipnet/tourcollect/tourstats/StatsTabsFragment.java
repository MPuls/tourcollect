package de.mipnet.tourcollect.tourstats;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import de.mipnet.tourcollect.R;

import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE;
import static de.mipnet.tourcollect.StaticVariables.WALK;

public class StatsTabsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stats_tabs, container, false); //returns a View
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Bundle args = getArguments();
        Resources resources = getResources();

        ViewPager viewPager = view.findViewById(R.id.stats_viewpager);
        String movingType = args != null ? args.getString(MOVING_TYPE) : WALK;
        StatsTabPagerAdapter adapterViewPager = new StatsTabPagerAdapter(getChildFragmentManager(), resources, movingType);
        viewPager.setAdapter(adapterViewPager);
    }
}

