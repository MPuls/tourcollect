package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;

import de.mipnet.tourcollect.FormatHelper;

public class DistanceLabelValueFormatter extends ValueFormatter {

    private final String unit;

    DistanceLabelValueFormatter(String unit) {
        this.unit = unit;
    }

    @Override
    public String getPointLabel(Entry entry) {
        return FormatHelper.formatDistanceSimple(entry.getY()) + " "+ unit;
    }
}
