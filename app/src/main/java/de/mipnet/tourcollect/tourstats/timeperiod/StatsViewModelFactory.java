package de.mipnet.tourcollect.tourstats.timeperiod;

import android.app.Application;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

/*
Using AndroidViewModel with a factor in order to inject parameters to the ViewModel's constructor
https://stackoverflow.com/questions/51829280/how-to-use-a-viewmodelprovider-factory-when-extends-from-androidviewmodel
 */
public class StatsViewModelFactory extends ViewModelProvider.NewInstanceFactory {
    static final private String TAG = "StatsViewModelFactory";

    @NonNull
    private final Application application;
    private final String movingType;
    private final String timePeriodUnit;


    public StatsViewModelFactory(@NonNull Application application, String movingType, String timePeriodUnit) {
        this.application = application;
        this.movingType = movingType;
        this.timePeriodUnit = timePeriodUnit;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        Log.d(TAG, "ViewModel create() called with: modelClass = [" + modelClass + "]");
        Log.d(TAG, "ViewModel create(): getInstance() called with movingType = [" + movingType  + "], timePeriodUnit = [" + timePeriodUnit + "]");
        if (modelClass.isAssignableFrom(StatsViewModel.class)) {
            //noinspection unchecked
            return (T) StatsViewModel.getInstance(application, movingType, timePeriodUnit);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}
