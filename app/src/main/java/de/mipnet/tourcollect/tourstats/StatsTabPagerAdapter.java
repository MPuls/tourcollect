package de.mipnet.tourcollect.tourstats;

import android.content.res.Resources;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsContentFragment;

import static de.mipnet.tourcollect.StaticVariables.DAY;
import static de.mipnet.tourcollect.StaticVariables.MONTH;
import static de.mipnet.tourcollect.StaticVariables.WEEK;
import static de.mipnet.tourcollect.StaticVariables.YEAR;

/*
Difference between FragmentStatePagerAdapter and FragmentPagerAdapter
https://stackoverflow.com/questions/18747975/what-is-the-difference-between-fragmentpageradapter-and-fragmentstatepageradapte
*/

public class StatsTabPagerAdapter extends FragmentPagerAdapter {

    private final String TAG = this.getClass().getSimpleName();

    private final Resources resources;
    private final String movingType;

    StatsTabPagerAdapter(FragmentManager fm, Resources resources, String movingType) {
        super(fm);
        this.resources = resources;
        this.movingType = movingType;
    }

    @Override
    public Fragment getItem(int position) {
        Log.d(TAG, "getItem: position: " + position);
        return switch (position) {
            case 1 -> StatsContentFragment.newInstance(movingType, WEEK);
            case 2 -> StatsContentFragment.newInstance(movingType, MONTH);
            case 3 -> StatsContentFragment.newInstance(movingType, YEAR);
            default -> StatsContentFragment.newInstance(movingType, DAY);
        };
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return switch (position) {
            case 1 -> resources.getString(R.string.week);
            case 2 -> resources.getString(R.string.month);
            case 3 -> resources.getString(R.string.year);
            default -> resources.getString(R.string.day);
        };
    }
}
