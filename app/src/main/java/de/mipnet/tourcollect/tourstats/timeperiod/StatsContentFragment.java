package de.mipnet.tourcollect.tourstats.timeperiod;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.tourstats.timeperiod.charts.ChartsFragment;
import de.mipnet.tourcollect.tourstats.timeperiod.sumup.SumUpFragment;

import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE;
import static de.mipnet.tourcollect.tourstats.StaticVariables.TIME_PERIOD_TYPE;

public class StatsContentFragment extends Fragment {
    final String TAG = this.getClass().getSimpleName();

    // instance variables
    private String movingType;
    private String timePeriodType;

    // newInstance constructor for creating fragment with arguments
    public static StatsContentFragment newInstance(String movingType, String timePeriodUnit) {
        StatsContentFragment statsContentFragment = new StatsContentFragment();
        Bundle args = new Bundle();
        args.putString(MOVING_TYPE, movingType);
        args.putString(TIME_PERIOD_TYPE, timePeriodUnit);
        statsContentFragment.setArguments(args);
        return statsContentFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        assert args != null;
        timePeriodType = args.getString(TIME_PERIOD_TYPE);
        movingType = args.getString(MOVING_TYPE);
        Log.d(TAG, "onCreate() called with: timePeriodType = [" + timePeriodType + "] | movingType = [" + movingType + "]");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView() called with: inflater = [" + inflater + "], container = [" + container + "], savedInstanceState = [" + savedInstanceState + "]");
        super.onCreateView(inflater, container, savedInstanceState);
        if (savedInstanceState == null) {
            SumUpFragment sumUpFragment = SumUpFragment.newInstance(movingType, timePeriodType);
            ChartsFragment chartsFragment = ChartsFragment.newInstance(movingType, timePeriodType);
            Log.d(TAG, "onViewCreated: sumUpFragment = [" + sumUpFragment + "], chartsFragment = [" + chartsFragment + "]");

            FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
            transaction.replace(R.id.stats_sumup, sumUpFragment);
            transaction.replace(R.id.stats_charts, chartsFragment);
            transaction.commit();
        }
        return inflater.inflate(R.layout.stats_content, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }
}
