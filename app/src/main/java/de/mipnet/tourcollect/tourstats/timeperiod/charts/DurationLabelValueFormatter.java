package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import android.util.Log;

import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;

import de.mipnet.tourcollect.FormatHelper;

public class DurationLabelValueFormatter extends ValueFormatter {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public String getPointLabel(Entry entry) {
        Log.d(TAG, "getBarLabel: value: " + entry.getY());
        return FormatHelper.formatDurationHm((long) entry.getY());
    }
}
