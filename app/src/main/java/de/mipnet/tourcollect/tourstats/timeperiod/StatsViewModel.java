package de.mipnet.tourcollect.tourstats.timeperiod;

import android.app.Application;
import android.util.Log;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import de.mipnet.tourcollect.datamodel.SumUpToursPojo;
import de.mipnet.tourcollect.datamodel.datarepository.SumUpToursRepository;

import static de.mipnet.tourcollect.StaticVariables.MONTH;
import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE_MAP;
import static de.mipnet.tourcollect.StaticVariables.TIME_PERIOD_TYPE_MAP;
import static de.mipnet.tourcollect.StaticVariables.WEEK;
import static de.mipnet.tourcollect.StaticVariables.YEAR;
import static de.mipnet.tourcollect.tourstats.StaticVariables.CHART_MAX_ENTRIES;
import static java.lang.Math.min;

public class StatsViewModel extends ViewModel {

    static final private String TAG = "StatsViewModel";

    static private final StatsViewModel[][] instanceTable = new StatsViewModel[MOVING_TYPE_MAP.size()][TIME_PERIOD_TYPE_MAP.size()];

    private final String movingType;
    private final String timePeriodType;
    private final SumUpToursRepository sumUpToursRepository;

    private final MutableLiveData<Integer> selectedPagerPosition = new MutableLiveData<>();
    private final MutableLiveData<Integer> selectedChartPoint = new MutableLiveData<>();
    private int pagerItemsCount;

    @SuppressWarnings("ConstantConditions")
    static public StatsViewModel getInstance(@NonNull Application application, @NonNull String movingType, @NonNull String timePeriodType) {
        Log.d(TAG, "getInstance() called with: application = [" + application + "], movingType = [" + movingType + "], timePeriodType = [" + timePeriodType + "]");
        Log.d(TAG, "getInstance: MOVING_TYPE_MAP.get(movingType) = [" + MOVING_TYPE_MAP.get(movingType) + "], TIME_PERIOD_TYPE_MAP.get(timePeriodType) = [" + TIME_PERIOD_TYPE_MAP.get(timePeriodType) + "]");
        StatsViewModel instanceTableEntry = instanceTable[MOVING_TYPE_MAP.get(movingType)][TIME_PERIOD_TYPE_MAP.get(timePeriodType)];
        Log.d(TAG, "getInstance: instanceTableEntry = [" + instanceTableEntry + "]");
        if (instanceTableEntry == null) {
            instanceTableEntry = new StatsViewModel(application, movingType, timePeriodType);
            instanceTable[MOVING_TYPE_MAP.get(movingType)][TIME_PERIOD_TYPE_MAP.get(timePeriodType)] = instanceTableEntry;
        }
        Log.d(TAG, "getInstance: instanceTableEntry = [" + instanceTableEntry + "]");
        return instanceTableEntry;
    }

    private StatsViewModel(@NonNull Application application, String movingType, String timePeriodType) {
        super();
        Log.d(TAG, "StatsViewModel() called with: application = [" + application + "], movingType = [" + movingType + "], timePeriodType = [" + timePeriodType + "]");
        this.movingType = movingType;
        this.timePeriodType = timePeriodType;
        this.sumUpToursRepository = SumUpToursRepository.getInstance(application);
    }

    public void setPagerItemsCount(int pagerItemCount) {
        Log.d(TAG, "setPagerItemsCount: pagerItemCount = [" + pagerItemCount + "]");
        this.pagerItemsCount = pagerItemCount;
    }

    public LiveData<List<SumUpToursPojo>> getSumUpToursByTimePeriodUnit() {
        return switch (timePeriodType) {
            case WEEK -> sumUpToursRepository.getSumUpToursByWeek(movingType);
            case MONTH -> sumUpToursRepository.getSumUpToursByMonth(movingType);
            case YEAR -> sumUpToursRepository.getSumUpToursByYear(movingType);
            default -> sumUpToursRepository.getSumUpToursByDay(movingType);
        };
    }

    public void setSelectedChartPoint(Integer chartPoint) {
        selectedChartPoint.setValue(chartPoint);
        int pagerPosition = pagerItemsCount - min(pagerItemsCount, CHART_MAX_ENTRIES) + chartPoint;
        Log.d(TAG, "setSelectedChartPoint: chartPoint = [" + chartPoint + "], pagerPosition = [" + pagerPosition + "], pagerItemsCount = [" + pagerItemsCount + "]");
        selectedPagerPosition.setValue(pagerPosition);
    }

    public MutableLiveData<Integer> getSelectedChartPoint() {
        return selectedChartPoint;
    }

    public void setSelectedPagerPosition(Integer pagerPosition) {
        selectedPagerPosition.setValue(pagerPosition);
        int chartPoint = min(pagerItemsCount, CHART_MAX_ENTRIES) - pagerItemsCount + pagerPosition;
        Log.d(TAG, "setSelectedPagerPosition: chartPoint = [" + chartPoint + "], pagerPosition = [" + pagerPosition + "], pagerItemsCount = [" + pagerItemsCount + "]");
        selectedChartPoint.setValue(chartPoint);
    }

    public MutableLiveData<Integer> getSelectedPagerPosition() {
        return selectedPagerPosition;
    }
}
