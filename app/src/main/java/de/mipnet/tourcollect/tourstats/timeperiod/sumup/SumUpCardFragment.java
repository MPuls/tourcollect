package de.mipnet.tourcollect.tourstats.timeperiod.sumup;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.sql.Timestamp;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.R;

import static de.mipnet.tourcollect.StaticVariables.HOUR;
import static de.mipnet.tourcollect.StaticVariables.MINUTE;
import static de.mipnet.tourcollect.tourstats.StaticVariables.AVERAGE_SPEED;
import static de.mipnet.tourcollect.tourstats.StaticVariables.DISTANCE;
import static de.mipnet.tourcollect.tourstats.StaticVariables.DURATION;
import static de.mipnet.tourcollect.tourstats.StaticVariables.TIME;
import static de.mipnet.tourcollect.tourstats.StaticVariables.TIME_PERIOD_TYPE;

public class SumUpCardFragment extends Fragment {

    private final String TAG = this.getClass().getSimpleName();

    public static SumUpCardFragment newInstance(String timePeriodType, long time, double distance, long duration, double averageSpeed) {
        SumUpCardFragment sumUpCardFragment = new SumUpCardFragment();
        Bundle args = new Bundle();
        args.putString(TIME_PERIOD_TYPE, timePeriodType);
        args.putLong(TIME, time);
        args.putDouble(DISTANCE, distance);
        args.putLong(DURATION, duration);
        args.putDouble(AVERAGE_SPEED, averageSpeed);
        sumUpCardFragment.setArguments(args);
        return sumUpCardFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.stats_time_card, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        Log.d(TAG, "onViewCreated: args: " + args);

        if (args != null) {
            String timePeriodType = args.getString(TIME_PERIOD_TYPE);
            if (timePeriodType == null) timePeriodType = "Unknown time period";
            Timestamp timestamp = new Timestamp(args.getLong(TIME));
            double distance = args.getDouble(DISTANCE);
            long duration = args.getLong(DURATION);
            double averageSpped = args.getDouble(AVERAGE_SPEED);

            TextView timeTv = view.findViewById(R.id.statsTimeCard_time_tv);
            TextView distanceTv = view.findViewById(R.id.statsTimeCard_distance_tv);
            TextView averageSpeedTv = view.findViewById(R.id.statsTimeCard_averageSpeedMoving_tv);
            TextView durationHoursTv = view.findViewById(R.id.statsTimeCard_durationMovingHoursValue_tv);
            TextView durationMinutesTv = view.findViewById(R.id.statsTimeCard_durationMovingMinutesValue_tv);

            timeTv.setText(FormatHelper.formatTimestampToTimePeriodUnit(timestamp, timePeriodType, getResources()));
            distanceTv.setText(FormatHelper.formatDistance(distance));
            averageSpeedTv.setText(FormatHelper.formatAverageSpeed(averageSpped));
            durationHoursTv.setText(FormatHelper.formatDuration(duration).get(HOUR));
            durationMinutesTv.setText(FormatHelper.formatDuration(duration).get(MINUTE));
        }
    }
}
