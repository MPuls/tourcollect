package de.mipnet.tourcollect.tourstats.timeperiod.sumup;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.viewpager.widget.ViewPager;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModel;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModelFactory;

import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE;
import static de.mipnet.tourcollect.tourstats.StaticVariables.TIME_PERIOD_TYPE;

public class SumUpFragment extends Fragment {
    final String TAG = this.getClass().getSimpleName();

    private StatsViewModel statsViewModel;

    // instance variables
    private String movingType;
    private String timePeriodType;

    // newInstance constructor for creating fragment with arguments
    public static SumUpFragment newInstance(String movingType, String timePeriodUnit) {
        SumUpFragment sumUpFragment = new SumUpFragment();
        Bundle args = new Bundle();
        args.putString(MOVING_TYPE, movingType);
        args.putString(TIME_PERIOD_TYPE, timePeriodUnit);
        sumUpFragment.setArguments(args);
        return sumUpFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        assert args != null;
        timePeriodType = args.getString(TIME_PERIOD_TYPE);
        movingType = args.getString(MOVING_TYPE);
        Log.d(TAG, "onCreate() called with: timePeriodType = [" + timePeriodType + "] | movingType = [" + movingType + "]");
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.stats_sumup, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        StatsViewModelFactory statsViewModelFactory = new StatsViewModelFactory(requireActivity().getApplication(), movingType, timePeriodType);
        statsViewModel = ViewModelProviders.of(this, statsViewModelFactory).get(StatsViewModel.class);
        Log.d(TAG, "statsViewModelFactory: " + statsViewModelFactory);
        Log.d(TAG, "statsViewModel: " + statsViewModel);

        ViewPager sumUpPagerView = view.findViewById(R.id.time_pager);

        statsViewModel.getSumUpToursByTimePeriodUnit().observe(getViewLifecycleOwner(), sumUpToursList -> {
            SumUpPagerAdapter sumUpPagerAdapter = new SumUpPagerAdapter(getChildFragmentManager(), sumUpToursList);
            sumUpPagerView.setAdapter(sumUpPagerAdapter);
            sumUpPagerView.addOnPageChangeListener(new OnPageChangeListener(statsViewModel));
            int pagerItemsCount = sumUpPagerAdapter.getCount();
            statsViewModel.setPagerItemsCount(pagerItemsCount);
            statsViewModel.setSelectedPagerPosition(pagerItemsCount - 1);
        });

        statsViewModel.getSelectedPagerPosition().observe(getViewLifecycleOwner(), sumUpPagerView::setCurrentItem);
    }
}
