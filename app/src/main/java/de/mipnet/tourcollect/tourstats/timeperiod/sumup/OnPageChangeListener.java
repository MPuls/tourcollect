package de.mipnet.tourcollect.tourstats.timeperiod.sumup;

import android.util.Log;

import androidx.viewpager.widget.ViewPager;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModel;

class OnPageChangeListener implements ViewPager.OnPageChangeListener {
    final private String TAG = this.getClass().getSimpleName();

    final private StatsViewModel statsViewModel;

    OnPageChangeListener(StatsViewModel statsViewModel) {
        this.statsViewModel = statsViewModel;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {
        //
    }

    @Override
    public void onPageSelected(int i) {
        Log.d(TAG, "onPageSelected: position i = [" + i + "]");
        statsViewModel.setSelectedPagerPosition(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        //
    }
}
