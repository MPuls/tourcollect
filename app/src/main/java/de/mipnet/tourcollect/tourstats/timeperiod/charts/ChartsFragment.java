package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.SumUpToursPojo;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModel;
import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModelFactory;

import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE;
import static de.mipnet.tourcollect.tourstats.StaticVariables.CHART_MAX_ENTRIES;
import static de.mipnet.tourcollect.tourstats.StaticVariables.TIME_PERIOD_TYPE;
import static java.lang.Math.min;

public class ChartsFragment extends Fragment {
    final String TAG = this.getClass().getSimpleName();

    // instance variables
    private String movingType;
    private String timePeriodType;
    ArrayList<LineChart> charts = new ArrayList<>();

    // newInstance constructor for creating fragment with arguments
    public static ChartsFragment newInstance(String movingType, String timePeriodUnit) {
        ChartsFragment chartsFragment = new ChartsFragment();
        Bundle args = new Bundle();
        args.putString(MOVING_TYPE, movingType);
        args.putString(TIME_PERIOD_TYPE, timePeriodUnit);
        chartsFragment.setArguments(args);
        return chartsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        assert args != null;
        timePeriodType = args.getString(TIME_PERIOD_TYPE);
        movingType = args.getString(MOVING_TYPE);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.stats_charts, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        StatsViewModelFactory statsViewModelFactory = new StatsViewModelFactory(requireActivity().getApplication(), movingType, timePeriodType);
        StatsViewModel statsViewModel = ViewModelProviders.of(this, statsViewModelFactory).get(StatsViewModel.class);
        Log.d(TAG, "statsViewModelFactory: " + statsViewModelFactory);
        Log.d(TAG, "statsViewModel: " + statsViewModel);

        LineChart chartAverageSpeed = view.findViewById(R.id.chart_averageSpeed);
        LineChart chartDistance = view.findViewById(R.id.chart_distance);
        LineChart chartDuration = view.findViewById(R.id.chart_duration);

        charts.add(chartAverageSpeed);
        charts.add(chartDistance);
        charts.add(chartDuration);

        view.post(new ChartsLayoutParamsRunnable(getResources(), view, charts));

        statsViewModel.getSumUpToursByTimePeriodUnit().observe(getViewLifecycleOwner(), sumUpToursList -> {

            OnChartValueSelectedListener onChartValueSelectedListener = new OnChartValueSelectedListener(statsViewModel, charts);

            if (sumUpToursList != null) {
                LineDataSet averageSpeedDataSet = generateAverageSpeedData(sumUpToursList);
                LineDataSet distanceSet = generateDistanceDataSet(sumUpToursList);
                LineDataSet durationSet = generateDurationDataSet(sumUpToursList);
                inflateChart(chartAverageSpeed, averageSpeedDataSet);
                inflateChart(chartDistance, distanceSet);
                inflateChart(chartDuration, durationSet);
                drawXAxis(chartAverageSpeed, XAxis.XAxisPosition.TOP, sumUpToursList);
                drawXAxis(chartDuration, XAxis.XAxisPosition.BOTTOM, sumUpToursList);
                drawXAxis(chartDistance, XAxis.XAxisPosition.BOTTOM_INSIDE, sumUpToursList);
                if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
                    chartDistance.getXAxis().setTextColor(Color.argb(0, 0, 0, 0));

                chartAverageSpeed.setOnChartValueSelectedListener(onChartValueSelectedListener);
                chartDistance.setOnChartValueSelectedListener(onChartValueSelectedListener);
                chartDuration.setOnChartValueSelectedListener(onChartValueSelectedListener);
            }
        });

        statsViewModel.getSelectedChartPoint().observe(getViewLifecycleOwner(), chartPointX -> {
            Log.d(TAG, "getSelectedChartPoint().observe: onChanged() called with: chartPointX = [" + chartPointX + "]");
            if (chartPointX != null && !chartAverageSpeed.isEmpty()) {
                chartAverageSpeed.highlightValue(chartPointX, 0, false);
                chartDuration.highlightValue(chartPointX, 0, false);
                chartDistance.highlightValue(chartPointX, 0, false);
            }
        });
    }

    private void drawXAxis(LineChart chart, XAxis.XAxisPosition position, List<SumUpToursPojo> sumUpToursList) {
        String[] xValues = generateXValues(sumUpToursList);
        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(true);
        xAxis.setValueFormatter(new XAxisValueFormatter(xValues));
        xAxis.setPosition(position);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setGranularityEnabled(true);
        xAxis.setGranularity(1f);
        xAxis.setTextSize(10f);
        xAxis.setAxisMinimum(-0.5f);
        xAxis.setAxisMaximum(min(sumUpToursList.size(),CHART_MAX_ENTRIES)-0.5f);
    }

    private void inflateChart(LineChart chart, LineDataSet set) {
        LineData lineData = new LineData(set);

        chart.setData(lineData);
        chart.setKeepPositionOnRotation(true);
        chart.setScaleEnabled(false);
        chart.setVisibleXRangeMaximum(10f);
        chart.moveViewToX(10);

        Description description = chart.getDescription();
        description.setText(set.getLabel());
        description.setTextColor(set.getColor());
        chart.setDescription(description);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setEnabled(false);
        float space = 0.2f;
        leftAxis.setAxisMaximum(chart.getYMax() + chart.getYMax()*space);
        leftAxis.setAxisMinimum(chart.getYMin() - chart.getYMax()*space);

        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);

        Legend legend = chart.getLegend();
        legend.setEnabled(false);

        XAxis xAxis = chart.getXAxis();
        xAxis.setEnabled(false);

        chart.invalidate();
    }

    private String[] generateXValues(List<SumUpToursPojo> sumUpToursList) {
        ArrayList<String> xValuesList = new ArrayList<>();

        for (int i = sumUpToursList.size() - min(sumUpToursList.size(), CHART_MAX_ENTRIES); i < sumUpToursList.size(); i++) {
            xValuesList.add(FormatHelper.formatTimestampToTimePeriodUnitChart(sumUpToursList.get(i).timestamp,timePeriodType, getResources()));
        }
        String[] xValues = new String[xValuesList.size()];
        xValues = xValuesList.toArray(xValues);
        return xValues;
    }

    private void formatSet(LineDataSet set, int color) {
        set.setLineWidth(4f);
        set.setColor(color);
        set.setCircleColor(color);
        set.setFillColor(color);
        set.setValueTextColor(color);
        set.setCircleRadius(5f);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setValueTextSize(8f);
        set.setDrawHorizontalHighlightIndicator(false);
        set.setHighLightColor(getResources().getColor(R.color.grey_74, requireContext().getTheme()));
    }

    private LineDataSet generateDistanceDataSet(List<SumUpToursPojo> sumUpToursList) {
        ArrayList<Entry> entries = new ArrayList<>();
        double distance;
        int k = 0;
        for (int i = sumUpToursList.size() - min(sumUpToursList.size(), CHART_MAX_ENTRIES); i < sumUpToursList.size(); i++) {
            distance = sumUpToursList.get(i).distance;
            entries.add(new Entry((float) k, (float) distance));
            k++;
        }

        LineDataSet set = new LineDataSet(entries, getResources().getString(R.string.distance));
        TypedValue typedColor = new TypedValue();
        requireActivity().getTheme().resolveAttribute(R.attr.colorPrimary, typedColor, true);
        formatSet(set, typedColor.data);
        set.setValueFormatter(new DistanceLabelValueFormatter(getResources().getString(R.string.km)));

        return set;
    }

    private LineDataSet generateDurationDataSet(List<SumUpToursPojo> sumUpToursList) {
        ArrayList<Entry> entries = new ArrayList<>();
        double duration;
        int k = 0;
        for (int i = sumUpToursList.size() - min(sumUpToursList.size(), CHART_MAX_ENTRIES); i < sumUpToursList.size(); i++) {
            duration = sumUpToursList.get(i).durationMoving;
            entries.add(new Entry((float) k, (float) duration));
            k++;
        }

        LineDataSet set = new LineDataSet(entries, getResources().getString(R.string.duration_moving));
        TypedValue typedColor = new TypedValue();
        requireActivity().getTheme().resolveAttribute(R.attr.colorPrimaryDark, typedColor, true);
        formatSet(set, typedColor.data);
        set.setValueFormatter(new DurationLabelValueFormatter());

        return set;
    }

    private LineDataSet generateAverageSpeedData(List<SumUpToursPojo> sumUpToursList) {
        ArrayList<Entry> entries = new ArrayList<>();
        double averageSpeed;
        int k = 0;
        for (int i = sumUpToursList.size() - min(sumUpToursList.size(), CHART_MAX_ENTRIES); i < sumUpToursList.size(); i++) {
            averageSpeed = Tour.getAverageSpeed(sumUpToursList.get(i).distance, sumUpToursList.get(i).durationMoving);
            entries.add(new Entry((float) k, (float) averageSpeed));
            k++;
        }

        LineDataSet set = new LineDataSet(entries, getResources().getString(R.string.averageSpeed));
        TypedValue typedColor = new TypedValue();
        requireActivity().getTheme().resolveAttribute(R.attr.colorAccent, typedColor, true);
        formatSet(set,typedColor.data);
        set.setValueFormatter(new AverageSpeedLabelValueFormatter(getResources().getString(R.string.kmh)));

        return set;
    }
}
