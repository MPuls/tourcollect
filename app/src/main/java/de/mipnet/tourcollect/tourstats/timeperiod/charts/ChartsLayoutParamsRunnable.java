package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;

import java.util.ArrayList;

import androidx.constraintlayout.widget.ConstraintLayout;

class ChartsLayoutParamsRunnable implements Runnable {

    private final Resources resources;
    private final View containerView;
    private final ArrayList<LineChart> charts;

    ChartsLayoutParamsRunnable(Resources resources, View containerView, ArrayList<LineChart> charts) {
        this.resources = resources;
        this.containerView = containerView;
        this.charts = charts;
    }

    @Override
    public void run() {
        ArrayList<ConstraintLayout.LayoutParams> oldLayoutParams = new ArrayList<>();
        ArrayList<ConstraintLayout.LayoutParams> newLayoutParams = new ArrayList<>();

        for (int i = 0; i < charts.size(); i++) {
            oldLayoutParams.add(i, (ConstraintLayout.LayoutParams) charts.get(i).getLayoutParams());
        }

        int chartWidth = oldLayoutParams.get(0).width;
        int newChartHeight;
        if (resources.getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            newChartHeight = (int) (containerView.getHeight() * 0.32);
        } else {
            newChartHeight = containerView.getHeight();
        }

        for (int i = 0; i < charts.size(); i++) {
            newLayoutParams.add(i, new ConstraintLayout.LayoutParams(chartWidth, newChartHeight));
            newLayoutParams.get(i).topToTop = oldLayoutParams.get(i).topToTop;
            newLayoutParams.get(i).bottomToTop = oldLayoutParams.get(i).bottomToTop;
            newLayoutParams.get(i).topToBottom = oldLayoutParams.get(i).topToBottom;
            newLayoutParams.get(i).bottomToTop = oldLayoutParams.get(i).bottomToTop;
            newLayoutParams.get(i).topToBottom = oldLayoutParams.get(i).topToBottom;
            newLayoutParams.get(i).bottomToBottom = oldLayoutParams.get(i).bottomToBottom;

            charts.get(i).setLayoutParams(newLayoutParams.get(i));
        }
    }
}
