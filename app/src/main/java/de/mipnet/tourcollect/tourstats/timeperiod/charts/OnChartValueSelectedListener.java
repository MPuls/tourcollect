package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;

import java.util.ArrayList;

import de.mipnet.tourcollect.tourstats.timeperiod.StatsViewModel;

class OnChartValueSelectedListener implements com.github.mikephil.charting.listener.OnChartValueSelectedListener {
    final private StatsViewModel statsViewModel;
    final private ArrayList<LineChart> charts;

    OnChartValueSelectedListener(StatsViewModel statsViewModel, ArrayList<LineChart> charts) {
        this.statsViewModel = statsViewModel;
        this.charts = charts;
    }
    @Override
    public void onValueSelected(Entry entry, Highlight h) {
        statsViewModel.setSelectedChartPoint((int) entry.getX());
        for (int i = 0; i < charts.size(); i++) {
            charts.get(i).highlightValue(h.getX(), 0, false);
        }
    }

    @Override
    public void onNothingSelected() {
        for (int i = 0; i < charts.size(); i++) {
            charts.get(i).highlightValue(null);
        }
    }
}
