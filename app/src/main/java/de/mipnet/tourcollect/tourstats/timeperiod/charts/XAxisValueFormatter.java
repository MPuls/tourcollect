package de.mipnet.tourcollect.tourstats.timeperiod.charts;

import android.util.Log;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.ValueFormatter;

import java.util.Arrays;

public class XAxisValueFormatter extends ValueFormatter {
    private final String TAG = this.getClass().getSimpleName();
    private final String[] mValues;

    XAxisValueFormatter(String[] values) {
        Log.d(TAG, "values: " + Arrays.toString(values));
        this.mValues = values;
    }
    @Override
    public String getAxisLabel(float value, AxisBase axis) {
        Log.d(TAG, "getAxisLabel: called");
        Log.d(TAG, "getAxisLabel: values: " + Arrays.toString(mValues));
        Log.d(TAG, "getAxisLabel: value: " + value);
        Log.d(TAG, "getAxisLabel: (int) value: " + (int) value);
        int i = (int) value;
        if (i < 0) {
            return "";
        } else if (i >= mValues.length) {
            return "";
        } else {
            return mValues[(int) value];
        }
    }
}
