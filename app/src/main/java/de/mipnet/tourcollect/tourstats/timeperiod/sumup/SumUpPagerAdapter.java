package de.mipnet.tourcollect.tourstats.timeperiod.sumup;

import java.sql.Timestamp;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import de.mipnet.tourcollect.datamodel.SumUpToursPojo;
import de.mipnet.tourcollect.datamodel.Tour;

// Since this is an object collection, use a FragmentStatePagerAdapter,
// and NOT a FragmentPagerAdapter.
public class SumUpPagerAdapter extends FragmentStatePagerAdapter {

    private final List<SumUpToursPojo> sumUpToursList;

    SumUpPagerAdapter(FragmentManager fm, List<SumUpToursPojo> sumUpToursList) {
        super(fm);
        this.sumUpToursList = sumUpToursList;
    }

    @Override
    public Fragment getItem(int position) {
        String timePeriodType = sumUpToursList.get(position).timePeriodType;
        Timestamp timestamp = sumUpToursList.get(position).timestamp;
        long duration = sumUpToursList.get(position).durationMoving;
        double distance = sumUpToursList.get(position).distance;
        double averageSpeed = Tour.getAverageSpeed(distance, duration);

        return SumUpCardFragment.newInstance(timePeriodType, timestamp.getTime(), distance, duration, averageSpeed);
    }

    @Override
    public int getCount() {
        return sumUpToursList.size();
    }
}
