package de.mipnet.tourcollect.currenttour.viewmodel;

public interface MetronomeListener {
    void onMetronomeTick(long duration);
}
