package de.mipnet.tourcollect.currenttour;

import android.content.Context;
import android.util.Log;

import de.mipnet.tourcollect.currenttour.viewmodel.Metronome;
import de.mipnet.tourcollect.currenttour.viewmodel.MetronomeListener;
import de.mipnet.tourcollect.datamodel.SpeedLimitsPojo;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

import static de.mipnet.tourcollect.StaticVariables.BIKE;
import static de.mipnet.tourcollect.StaticVariables.CAR;
import static de.mipnet.tourcollect.StaticVariables.RUN;
import static de.mipnet.tourcollect.StaticVariables.WALK;

class StoreOnMetronomeTick implements MetronomeListener {
    private final String TAG = this.getClass().getSimpleName();
    private final TourRepository tourRepository;
    private final Metronome metronome;
    private final SpeedLimitsPojo speedLimits;

    StoreOnMetronomeTick(Context applicationContext) {
        this.tourRepository = TourRepository.getInstance(applicationContext);;
        this.speedLimits = new SpeedLimitsPojo(applicationContext);
        this.metronome = new Metronome();
        metronome.addListener(this);
    }

    public void start() {
        metronome.start();
    }

    public void pause() {
        metronome.stop();
    }

    public void destroy() {
        if (metronome != null) {
            metronome.stop();
            metronome.removeListener(this);
        }
    }

    @Override
    public void onMetronomeTick(long duration) {
        Log.d(TAG, "onMetronomeTick: triggered");
        Tour currentTour = tourRepository.getLastTour();
        if (currentTour.isRunningStatus()) {
            currentTour.setDurationTour(currentTour.getDurationTour() + duration);
            if (currentTour.getCurrentSpeed() >= getSpeedLimit()) {
                currentTour.setDurationMoving(currentTour.getDurationMoving() + duration);
                currentTour.setMovingType(getMovingType(currentTour.getAverageSpeedMoving()));
            }
            tourRepository.updateTour(currentTour);
        }
    }

    private double getSpeedLimit() {
        return speedLimits.getSpeedLimitMoving() * 1000 / 3600;
    }

    private String getMovingType(double averageSpeedMoving) {
        double speedLimitCar = speedLimits.getSpeedLimitCar()/3600d;
        double speedLimitBike = speedLimits.getSpeedLimitBike()/3600d;
        double speedLimitRun = speedLimits.getSpeedLimitRun()/3600d;
        double speedLimitWalk = speedLimits.getSpeedLimitWalk()/3600d;

        if (averageSpeedMoving >= speedLimitCar) {
            return CAR;
        } else if (averageSpeedMoving >= speedLimitBike) {
            return BIKE;
        } else if (averageSpeedMoving >= speedLimitRun) {
            return RUN;
        } else if (averageSpeedMoving >= speedLimitWalk) {
            return WALK;
        }
        return WALK;
    }
}
