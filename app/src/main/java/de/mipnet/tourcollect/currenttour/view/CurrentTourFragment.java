package de.mipnet.tourcollect.currenttour.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.PermissionHandler;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.currenttour.ForegroundService;
import de.mipnet.tourcollect.currenttour.viewmodel.CurrentTourVM;
import de.mipnet.tourcollect.datamodel.Tour;

import static de.mipnet.tourcollect.StaticVariables.BIKE;
import static de.mipnet.tourcollect.StaticVariables.CAR;
import static de.mipnet.tourcollect.StaticVariables.HOUR;
import static de.mipnet.tourcollect.StaticVariables.MINUTE;
import static de.mipnet.tourcollect.StaticVariables.RUN;
import static de.mipnet.tourcollect.StaticVariables.SECOND;

public class CurrentTourFragment extends Fragment {

    private final String TAG = this.getClass().getSimpleName();

    private SharedPreferences sharedPreferences;

    private TextView tvSpeed, tvDistance, tvAverageSpeedMoving, tvAverageSpeedTour, tvDurationMovingHours,tvDurationMovingMinutes, tvDurationMovingSeconds, tvDurationTourHours, tvDurationTourMinutes, tvDurationTourSeconds;
    private FloatingActionButton btnStartPauseTour, btnNewTour;
    private ImageView imgMovingType;
    private CurrentTourVM currentTourVM;
    final String START = "START";
    final String PAUSE = "PAUSE";
    private boolean previousRunningStatus = false;
    
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        Log.i(TAG, "onCreateView: lifecycle");
        return inflater.inflate(R.layout.current_tour, container, false); //returns a View
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Log.i(TAG, "onViewCreated: lifecycle");
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        tvSpeed = view.findViewById(R.id.currentTour_currentSpeed_tv);
        tvDurationMovingHours = view.findViewById(R.id.currentTour_durationMovingHoursValue_tv);
        tvDurationMovingMinutes =  view.findViewById(R.id.currentTour_durationMovingMinutesValue_tv);
        tvDurationMovingSeconds =  view.findViewById(R.id.currentTour_durationMovingSecondsValue_tv);
        tvDurationTourHours = view.findViewById(R.id.currentTour_durationTourHoursValue_tv);
        tvDurationTourMinutes =  view.findViewById(R.id.currentTour_durationTourMinutesValue_tv);
        tvDurationTourSeconds =  view.findViewById(R.id.currentTour_durationTourSecondsValue_tv);
        tvDistance = view.findViewById(R.id.currentTour_distance_tv);
        tvAverageSpeedMoving =  view.findViewById(R.id.currentTour_averageSpeedMoving_tv);
        tvAverageSpeedTour = view.findViewById(R.id.currentTour_averageSpeedTour_tv);
        btnStartPauseTour =  view.findViewById(R.id.currentTour_StartPauseTour_btn);
        btnNewTour =  view.findViewById(R.id.currentTour_NewTour_btn);
        imgMovingType =  view.findViewById(R.id.currentTour_movingType_img);

        keepScreenOn();
        handlingStartPauseButton(false);
    }

    @Override
    public void onStart() {
        Log.i(TAG, "onStart: lifecycle");
        super.onStart();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume: lifecycle");
        super.onResume();
        currentTourVM = new ViewModelProvider(this).get(CurrentTourVM.class);

        handlingStartPauseButton(previousRunningStatus);

        // ### Observer of CurrentTourData ###
        final Observer<Tour> currentTourObserver = (@Nullable final Tour newTourData) -> {
            boolean runningStatus;
            if (newTourData != null) {
                tvSpeed.setText(FormatHelper.formatSpeed(newTourData.getCurrentSpeed()));
                tvDistance.setText(FormatHelper.formatDistance(newTourData.getDistance()));
                tvDurationTourHours.setText(FormatHelper.formatDuration(newTourData.getDurationTour()).get(HOUR));
                tvDurationTourMinutes.setText(FormatHelper.formatDuration(newTourData.getDurationTour()).get(MINUTE));
                tvDurationTourSeconds.setText(FormatHelper.formatDuration(newTourData.getDurationTour()).get(SECOND));
                tvDurationMovingHours.setText(FormatHelper.formatDuration(newTourData.getDurationMoving()).get(HOUR));
                tvDurationMovingMinutes.setText(FormatHelper.formatDuration(newTourData.getDurationMoving()).get(MINUTE));
                tvDurationMovingSeconds.setText(FormatHelper.formatDuration(newTourData.getDurationMoving()).get(SECOND));
                tvAverageSpeedMoving.setText(FormatHelper.formatAverageSpeed(newTourData.getAverageSpeedMoving()));
                tvAverageSpeedTour.setText(FormatHelper.formatAverageSpeed(newTourData.getAverageSpeedTour()));
                handlingMovingTypeImage(newTourData.getMovingType());
                runningStatus = newTourData.isRunningStatus();
                Log.v(TAG, "previousRunningStatus / runningStatus: " + previousRunningStatus + " / " + runningStatus);
                if (runningStatus != previousRunningStatus){
                    handlingStartPauseButton(runningStatus);
                    previousRunningStatus = runningStatus;
                }
            }
        };
        currentTourVM.getCurrentTourLiveData().observe(requireActivity(),currentTourObserver);
    }

    @Override
    public void onPause() {
        Log.i(TAG, "onPause: lifecycle");
        super.onPause();
    }

    @Override
    public void onStop() {
        Log.i(TAG, "onStop: lifecycle");
        super.onStop();
    }

    @Override
    public void onDestroyView() {
        Log.i(TAG, "onDestroyView: lifecycle");
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        Log.i(TAG, "onDestroy: lifecycle");
        super.onDestroy();
    }

    @Override
    public void onDetach() {
        Log.i(TAG, "onDetach: lifecycle");
        super.onDetach();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.current_tour_toolbar, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        var isGlobalFontSizeSwitchOn = sharedPreferences.getBoolean(getResources().getString(R.string.prefs_change_global_font_size_key), false);
        menu.findItem(R.id.currentTour_optionsMenu_globalFontSize).setChecked(isGlobalFontSizeSwitchOn);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.currentTour_optionsMenu_globalFontSize) {
            var isGlobalFontSizeSwitchOn = sharedPreferences.getBoolean(getResources().getString(R.string.prefs_change_global_font_size_key), false);
            Log.d(TAG, "onOptionsItemSelected: isGlobalFontSizeSwitchOn: before: " + isGlobalFontSizeSwitchOn);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putBoolean(getResources().getString(R.string.prefs_change_global_font_size_key), !isGlobalFontSizeSwitchOn);
            if (!isGlobalFontSizeSwitchOn && PermissionHandler.isWriteSettingsPermissionGranted(getActivity())) {
                editor.apply();
            } else if (isGlobalFontSizeSwitchOn) {
                editor.apply();
            } else {
                Log.d(TAG, "onOptionsItemSelected: not granted: " + Manifest.permission.WRITE_SETTINGS);
            }
            isGlobalFontSizeSwitchOn = sharedPreferences.getBoolean(getResources().getString(R.string.prefs_change_global_font_size_key), false);
            Log.d(TAG, "onOptionsItemSelected: isGlobalFontSizeSwitchOn: after: " + isGlobalFontSizeSwitchOn);
            item.setChecked(isGlobalFontSizeSwitchOn);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *  Handling the images of floating buttons and defines the onClickListener for the buttons all in life-cycle-awareness.
     */
    // TODO: remove linter suppression
    @SuppressLint("RestrictedApi")
    private void handlingStartPauseButton(boolean runningStatus) {
        if (runningStatus) {
            Log.v(TAG, "current tour is running --> start/pause button should show pause and the new button should be invisible");
            btnStartPauseTour.setOnClickListener(v -> pauseTour());
            // https://stackoverflow.com/questions/35121147/change-android-drawable-button-icon-programmatically
            btnStartPauseTour.setImageResource(android.R.drawable.ic_media_pause);
            btnStartPauseTour.setTag(R.id.pause);
            btnNewTour.setVisibility(View.INVISIBLE);
        } else {
            Log.v(TAG,"current tour is paused or not started yet --> start/pause button should show play and the new button should be visible.");
            btnStartPauseTour.setOnClickListener(v -> startTour());
            btnStartPauseTour.setImageResource(android.R.drawable.ic_media_play);
            btnStartPauseTour.setTag(R.id.play);
            btnNewTour.setVisibility(View.VISIBLE);
        }
        btnNewTour.setOnClickListener(v -> newTour());
    }

    private void handlingMovingTypeImage(String movingType) {
        switch (movingType) {
            case RUN -> imgMovingType.setImageResource(R.drawable.ic_run);
            case BIKE -> imgMovingType.setImageResource(R.drawable.ic_bike);
            case CAR -> imgMovingType.setImageResource(R.drawable.ic_car);
            default -> imgMovingType.setImageResource(R.drawable.ic_walk);
        }
    }

    private Float seekBarValueToFontScale(Integer seekBarValue) {
        return switch (seekBarValue) {
            case 0 -> 0.8f;
            case 2 -> 1.2f;
            case 3 -> 1.4f;
            default -> 1.0f;
        };
    }

    private void setFontScale(String action) {
        var isGlobalFontSizeSwitchOn = sharedPreferences.getBoolean("globalFontSizeSwitch", false);
        if (isGlobalFontSizeSwitchOn) {
            if (PermissionHandler.isWriteSettingsPermissionGranted(getActivity())) {
                Log.i(TAG, "setFontScale: old font scale: " + getResources().getConfiguration().fontScale);
                Float newFontScale = null;
                int seekBarValue;
                switch (action) {
                    case START -> {
                        seekBarValue = sharedPreferences.getInt(getResources().getString(R.string.prefs_font_size_tour_running_key), 1);
                        newFontScale = seekBarValueToFontScale(seekBarValue);
                        Settings.System.putString(requireActivity().getContentResolver(), Settings.System.FONT_SCALE, newFontScale.toString());
                    }
                    case PAUSE -> {
                        seekBarValue = sharedPreferences.getInt(getResources().getString(R.string.prefs_font_size_tour_paused_key), 1);
                        newFontScale = seekBarValueToFontScale(seekBarValue);
                        Settings.System.putString(requireActivity().getContentResolver(), Settings.System.FONT_SCALE, newFontScale.toString());
                    }
                    default -> {
                    }
                }
                Log.i(TAG, "setFontScale: new font scale: " + newFontScale);
            } else {
                Log.d(TAG, "setFontScale: not granted: " + Manifest.permission.WRITE_SETTINGS);
            }
        }
    }

    private void startTour() {
        Log.i(TAG, "Start Tour");
        setFontScale(START);
        startForegroundService();
        currentTourVM.start();
    }

    private void pauseTour() {
        Log.i(TAG, "Pause Tour");
        setFontScale(PAUSE);
        stopForegroundService();
        currentTourVM.pause();

    }

    private void newTour() {
        Log.i(TAG, "Start new tour");
        currentTourVM.newCurrentTour();
    }

    private void keepScreenOn() {
        requireActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void startForegroundService() {
        if (PermissionHandler.isForegroundServicePermissionGranted(getActivity())) {
            var isForegroundServiceRunning = isMyServiceRunning(ForegroundService.class.getName());
            Log.i(TAG, "startForegroundService: foreground-service is running: " + isForegroundServiceRunning);
            if (!isForegroundServiceRunning) {
                Log.i(TAG, "startForegroundService: start foreground-service");
                requireActivity().startForegroundService(new Intent(requireContext(), ForegroundService.class));
            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) Log.w(TAG, "startForegroundService: not granted: " + Manifest.permission.FOREGROUND_SERVICE);
                Log.w(TAG, "startForegroundService: not granted: " + "FOREGROUND_SERVICE");
            }
        }
    }

    private void stopForegroundService() {
        var isForegroundServiceRunning = isMyServiceRunning(ForegroundService.class.getName());
        Log.i(TAG, "stopForegroundService: foreground-service is running: " + isForegroundServiceRunning);
        if (isForegroundServiceRunning) {
            Log.d(TAG, "stopForegroundService: stop foreground-service");
            requireActivity().stopService(new Intent(requireContext(), ForegroundService.class));
        }
    }

    // https://stackoverflow.com/questions/600207/how-to-check-if-a-service-is-running-on-android#5921190
    private boolean isMyServiceRunning(String serviceClassName) {
        ActivityManager manager = (ActivityManager) requireActivity().getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClassName.equals(service.service.getClassName())) {
                    Log.i(TAG, "service " + serviceClassName + " is already running.");
                    return true;
                }
            }
        }
        Log.i(TAG, "Service " + serviceClassName + " is not running.");
        return false;
    }
}
