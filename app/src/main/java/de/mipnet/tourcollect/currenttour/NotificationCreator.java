package de.mipnet.tourcollect.currenttour;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;

import androidx.core.app.NotificationCompat;
import de.mipnet.tourcollect.MainActivity;
import de.mipnet.tourcollect.R;

class NotificationCreator {
    private final Context applicationContext;
    private final Resources resources;
    private final static String CHANNEL_ID = "CURRENT_TOUR_CHANNEL";

    public NotificationCreator(Context applicationContext) {
        this.applicationContext = applicationContext;
        this.resources = applicationContext.getResources();

        createNotificationChannel();
    }

    public Notification getNotification() {
        var pendingIntent = getNotificationContentIntent();
        return new NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(resources.getString(R.string.currentTour))
                .setContentText(resources.getString(R.string.is_running))
                .setPriority(NotificationCompat.PRIORITY_LOW)
                .setContentIntent(pendingIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PRIVATE)
                .setOnlyAlertOnce(true)
                .setOngoing(true)
                .build();
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelName = resources.getString(R.string.notification_channel_current_tour_name);
            String channelDescription = resources.getString(R.string.notification_channel_current_tour_description);
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, channelName, android.app.NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(channelDescription);
            // Register the channel with the system; you can't change the importance or other notification behaviors after this
            android.app.NotificationManager notificationManager = applicationContext.getSystemService(android.app.NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private PendingIntent getNotificationContentIntent() {
        Intent tapNotificationIntent = new Intent(applicationContext, MainActivity.class);

        PendingIntent pendingIntent;
        int pendingIntentFlag;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.S) {
            pendingIntentFlag = PendingIntent.FLAG_MUTABLE | PendingIntent.FLAG_UPDATE_CURRENT;
        } else {
            pendingIntentFlag = PendingIntent.FLAG_UPDATE_CURRENT;
        }
        pendingIntent = PendingIntent.getActivity(applicationContext, 0, tapNotificationIntent, pendingIntentFlag);

        return pendingIntent;

    }
}
