package de.mipnet.tourcollect.currenttour;

import android.location.Location;
import android.util.Log;

import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

public class StoreOnLocationChange {
    private final String TAG = this.getClass().getSimpleName();
    private final TourRepository tourRepository;
    private final double speedLimitMoving;
    private Location lastLocation = null;

    public StoreOnLocationChange(TourRepository tourRepository, double speedLimitMoving) {
        this.tourRepository = tourRepository;
        this.speedLimitMoving = speedLimitMoving;
    }

    public void onLocationChange(Location location) {
        Tour currentTour = tourRepository.getLastTour();

        var currentTourIsRunning = currentTour.isRunningStatus();
        double accuracy = location.getAccuracy();
        double currentSpeed = location.getSpeed();

        if (currentTourIsRunning) {
            if (lastLocation != null && currentSpeed >= speedLimitMoving *1000/3600) {
                double distanceDelta = location.distanceTo(lastLocation);
                Log.i(TAG, "accuracy: " + accuracy
                                + " | currentSpeed: " + currentSpeed
                                + " | distanceDelta: " + distanceDelta);
                currentTour.setDistance(currentTour.getDistance() + distanceDelta);
            }
            currentTour.setCurrentSpeed(currentSpeed);
            tourRepository.updateTour(currentTour);
        }
        lastLocation = location;
    }
}
