package de.mipnet.tourcollect.currenttour.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import de.mipnet.tourcollect.currenttour.StoreCurrentTourLifeCycle;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;


public class CurrentTourVM extends AndroidViewModel {
    private LiveData<Tour> currentTourLiveData;
    private final StoreCurrentTourLifeCycle storeCurrentTourLifeCycle;

    public CurrentTourVM(@NonNull Application application) {
        super(application);


        TourRepository tourRepository = TourRepository.getInstance(application);
        storeCurrentTourLifeCycle = new StoreCurrentTourLifeCycle(tourRepository);

        try {
           currentTourLiveData = tourRepository.getLastTourLiveData();
        } catch (IndexOutOfBoundsException e) {
            // When starting the app for the first time, there would not be a last tour and getLastTourLiveData throws an IndexOutOfBoundException.
            newCurrentTour();
        }
    }

    public void start() {
        var currentTour = currentTourLiveData.getValue();
        if (currentTour != null) {
            storeCurrentTourLifeCycle.start(currentTourLiveData.getValue());
        } else {
            newCurrentTour();
        }
    }

    public void pause() {
        var currentTour = currentTourLiveData.getValue();
        if (currentTour != null) {
            storeCurrentTourLifeCycle.pause(currentTourLiveData.getValue());
        } else {
            newCurrentTour();
        }
    }

    public void newCurrentTour() {
        storeCurrentTourLifeCycle.createNewTour();
    }

    public LiveData<Tour> getCurrentTourLiveData(){
        return currentTourLiveData;
    }

}
