package de.mipnet.tourcollect.currenttour.viewmodel;

import android.util.Log;

import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Metronome {

    private final String TAG = this.getClass().getSimpleName();
    private static ScheduledExecutorService scheduler; // scheduler to update the metronome
    private final ArrayList<MetronomeListener> listeners = new ArrayList<>();
    private long oldTime = 0;

    public void addListener(MetronomeListener listener) {
        listeners.add(listener);
    }
    public void removeListener(MetronomeListener listener) {listeners.remove(listener);}

    private void informListener() {
        for(MetronomeListener listener : listeners) {
            listener.onMetronomeTick(getElapsedTime());
        }
    }

    public void start() {
        oldTime = 0;
        scheduler = Executors.newScheduledThreadPool(1);
        Runnable updateTime = this::informListener;
        scheduler.scheduleAtFixedRate(updateTime, 0, 1, TimeUnit.SECONDS);
    }

    public void stop() {
        if (scheduler != null) {
            scheduler.shutdown();
        } else {
            Log.w(TAG, "stopUpdateDurationsPeriodically: scheduler is null! Not able to shutdown.");
        }
    }

    public long getElapsedTime() {
        long now = System.currentTimeMillis();
        if (oldTime == 0) { oldTime = now; }
        long elapsedTime = now - oldTime;
        oldTime = now;
        return elapsedTime;
    }
}
