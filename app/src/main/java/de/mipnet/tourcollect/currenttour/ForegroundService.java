package de.mipnet.tourcollect.currenttour;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;

import de.mipnet.tourcollect.LocationUpdateHandler;

public class ForegroundService extends Service {

    private final static int NOTIFY_ID = 101;
    private StoreOnMetronomeTick storeOnMetronomeTick;
    private LocationUpdateHandler locationUpdateHandler;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context applicationContext = getApplicationContext();
        storeOnMetronomeTick = new StoreOnMetronomeTick(applicationContext);
        locationUpdateHandler = LocationUpdateHandler.getInstance(applicationContext);

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

        // Start service in foreground with a notification
        var notificationCreator = new NotificationCreator(getApplicationContext());
        var notification = notificationCreator.getNotification();
        startForeground(NOTIFY_ID, notification);

        // Start listening to location updates
        locationUpdateHandler.startLocationListening();

        // Start storing on time
        storeOnMetronomeTick.start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        storeOnMetronomeTick.pause();
        storeOnMetronomeTick.destroy();
        stopForeground(true);
        // Stop listening to location updates
        locationUpdateHandler.stopLocationListening();

    }
}
