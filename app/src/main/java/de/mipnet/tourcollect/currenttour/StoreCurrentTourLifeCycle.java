package de.mipnet.tourcollect.currenttour;

import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

public class StoreCurrentTourLifeCycle {
    private final TourRepository tourRepository;

    public StoreCurrentTourLifeCycle(TourRepository tourRepository) {
        this.tourRepository = tourRepository;
    }

    public void start(Tour currentTour) {
        currentTour.setRunningStatus(true);
        tourRepository.updateTour(currentTour);
    }

    public void pause(Tour currentTour) {
        currentTour.setRunningStatus(false);
        tourRepository.updateTour(currentTour);
    }

    public Tour createNewTour() {
        var currentTour = new Tour();
        long rowId = tourRepository.insertNewTour(currentTour);
        currentTour.setId(rowId);
        return currentTour;
    }
}
