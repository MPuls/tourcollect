package de.mipnet.tourcollect;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;

import java.util.Arrays;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import static android.provider.Settings.ACTION_MANAGE_WRITE_SETTINGS;

public class PermissionHandler implements ActivityCompat.OnRequestPermissionsResultCallback {

    private static final String TAG = "PermissionHandler";

    private static final int LOCATION_REQUEST_CODE = 0;
    private static final int WRITE_SETTINGS_REQUEST_CODE = 2;
    private static final int FOREGROUND_SERVICE_REQUEST_CODE = 3;

    private static final String[] permissionsName = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            "FOREGROUND_SERVICE"};

    private static boolean locationGranted = false;
    private static boolean writeSettingsGranted = false;
    private static boolean foregroundServiceGranted = false;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called: " + requestCode + Arrays.toString(permissions) + Arrays.toString(grantResults));
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case LOCATION_REQUEST_CODE: locationGranted = true;
                case WRITE_SETTINGS_REQUEST_CODE: writeSettingsGranted = true;
                case FOREGROUND_SERVICE_REQUEST_CODE: foregroundServiceGranted = true;
            }
            Log.d(TAG, "onRequestPermissionsResult: granted: " + permissionsName[requestCode]);
        }
    }

    public static boolean isLocationPermissionGranted(Activity activity) {
        locationGranted = false;
        if (!isGranted(activity, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // TODO shouldShowRequestPermissionRationale()
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        } else {
           /*
           LocationSettingsRequest is not performed here, since it is a Google API and the App should not depend on that
           https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient / https://stackoverflow.com/a/29872703
           */
            locationGranted = true;
        }
        return locationGranted;
    }

    //only if the app targets API level 28 (Android 9) or higher
    public static boolean isForegroundServicePermissionGranted(Activity activity) {
        foregroundServiceGranted = false;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            if (!isGranted(activity, Manifest.permission.FOREGROUND_SERVICE)) {
                // TODO shouldShowRequestPermissionRationale()
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.FOREGROUND_SERVICE}, FOREGROUND_SERVICE_REQUEST_CODE);
            } else {
                foregroundServiceGranted = true;
            }
        } else {
            foregroundServiceGranted = true;
        }
        return foregroundServiceGranted;
    }

    public static boolean isWriteSettingsPermissionGranted(Context applicationContext) {
        writeSettingsGranted = false;
        boolean canWriteSystemSettings = Settings.System.canWrite(applicationContext);
        if (!canWriteSystemSettings) {
            new AlertDialog.Builder(applicationContext)
                .setTitle(applicationContext.getString(R.string.prefs_write_system_settings_dialog_title))
                .setMessage(applicationContext.getString(R.string.pref_write_system_settings_dialog_message))
                .setPositiveButton(applicationContext.getString(R.string.dlg_ok), (dialog, which) -> {
                    Intent intent = new Intent(ACTION_MANAGE_WRITE_SETTINGS);
                    applicationContext.startActivity(intent);
                })
                .setNegativeButton(applicationContext.getString(R.string.dlg_cancel), null)
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
        } else {
            writeSettingsGranted = true;
        }
        Log.i(TAG, Manifest.permission.WRITE_SETTINGS + ": canWriteSystemSettings: " + Settings.System.canWrite(applicationContext));
        return writeSettingsGranted;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private static boolean isGranted(Context applicationContext, String permission) {
        boolean isGranted = ActivityCompat.checkSelfPermission(applicationContext, permission) == PackageManager.PERMISSION_GRANTED;
        Log.d(TAG, permission + " isGranted: " + isGranted);
        return isGranted;
    }
}
