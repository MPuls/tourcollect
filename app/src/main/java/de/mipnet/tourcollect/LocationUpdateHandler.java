package de.mipnet.tourcollect;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import de.mipnet.tourcollect.currenttour.StoreOnLocationChange;
import de.mipnet.tourcollect.datamodel.SpeedLimitsPojo;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

public class LocationUpdateHandler implements LocationListener {
    final String TAG = this.getClass().getSimpleName();
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // in meter
    private static final long MIN_TIME_BW_UPDATES = 500; // in milliseconds

    private final Context applicationContext;
    private static LocationUpdateHandler locationUpdateHandler;
    private final LocationManager locationManager;
    private final StoreOnLocationChange storeOnLocationChange;

    private LocationUpdateHandler(Context applicationContext) {
        this.applicationContext = applicationContext;
        locationManager = (LocationManager) applicationContext.getSystemService(Context.LOCATION_SERVICE);
        var tourRepository = TourRepository.getInstance(applicationContext);
        var speedLimits = new SpeedLimitsPojo(applicationContext);
        storeOnLocationChange = new StoreOnLocationChange(tourRepository,speedLimits.getSpeedLimitMoving());
    }

    public static LocationUpdateHandler getInstance(Context applicationContext) {
        if (locationUpdateHandler == null) {
            locationUpdateHandler = new LocationUpdateHandler(applicationContext);
        }
        return locationUpdateHandler;
    }

    public void startLocationListening() {
        if (locationManager != null) {
            if (ActivityCompat.checkSelfPermission(
                    applicationContext,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(applicationContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
            }
            locationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER,
                    MIN_TIME_BW_UPDATES,
                    MIN_DISTANCE_CHANGE_FOR_UPDATES,
                    this
            );
        }
    }

    public void stopLocationListening() {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    @Override
    public void onLocationChanged(@NonNull Location location) {
        Log.d(TAG, "onLocationChanged: location: " + location);
        storeOnLocationChange.onLocationChange(location);
    }

    @Override
    public void onProviderEnabled(@NonNull String provider) {
        LocationListener.super.onProviderEnabled(provider);

        Log.e(TAG, "onProviderEnabled: " + provider + " is enabled");
        Toast.makeText(
                applicationContext,
                applicationContext.getString(R.string.app_name) + ": " + provider + " " + applicationContext.getString(R.string.toast_gps_enabled),
                Toast.LENGTH_SHORT
        ).show();
    }

    @Override
    public void onProviderDisabled(@NonNull String provider) {
        LocationListener.super.onProviderDisabled(provider);

        Log.e(TAG, "onProviderDisabled: " + provider + " seems not to be enabled");
        Toast.makeText(
                applicationContext,
                applicationContext.getString(R.string.app_name) + ": " + provider + " " + applicationContext.getString(R.string.toast_gps_disabled),
                Toast.LENGTH_SHORT
        ).show();
    }
}
