package de.mipnet.tourcollect.datamodel.database;

import androidx.room.Dao;
import androidx.room.RawQuery;
import androidx.sqlite.db.SupportSQLiteQuery;

@Dao
public interface DatabaseHelperDao {

    @RawQuery
    int checkpoint(SupportSQLiteQuery supportSQLiteQuery);

}
