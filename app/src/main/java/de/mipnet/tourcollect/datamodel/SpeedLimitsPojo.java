package de.mipnet.tourcollect.datamodel;

import android.content.Context;

import androidx.preference.PreferenceManager;
import de.mipnet.tourcollect.R;

public class SpeedLimitsPojo {
    private final double speedLimitMoving;
    private final double speedLimitCar;
    private final double speedLimitBike;
    private final double speedLimitRun;
    private final double speedLimitWalk;

    public SpeedLimitsPojo(Context context) {
        var sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        var resources = context.getResources();
        this.speedLimitMoving = Double.parseDouble(
                sharedPreferences.getString(resources.getString(R.string.prefs_speedLimitMoving_key), "4")
        );
        this.speedLimitCar = Double.parseDouble(
                sharedPreferences.getString(resources.getString(R.string.prefs_speedLimitCar_key), "23")
        );
        this.speedLimitBike = Double.parseDouble(
                sharedPreferences.getString(resources.getString(R.string.prefs_speedLimitBike_key), "11")
        );
        this.speedLimitRun = Double.parseDouble(
                sharedPreferences.getString(resources.getString(R.string.prefs_speedLimitRun_key), "7")
        );
        this.speedLimitWalk = Double.parseDouble(
                sharedPreferences.getString(resources.getString(R.string.prefs_speedLimitWalk_key), "4")
        );
    }

    public double getSpeedLimitMoving() {
        return speedLimitMoving;
    }

    public double getSpeedLimitCar() {
        return speedLimitCar;
    }

    public double getSpeedLimitBike() {
        return speedLimitBike;
    }

    public double getSpeedLimitRun() {
        return speedLimitRun;
    }

    public double getSpeedLimitWalk() {
        return speedLimitWalk;
    }
}
