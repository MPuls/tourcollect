package de.mipnet.tourcollect.datamodel.database;

import java.sql.Timestamp;

import androidx.room.TypeConverter;

public class Converters {
    @TypeConverter
    public static Timestamp fromLong(Long value) {
        return new Timestamp(value);
    }
    @TypeConverter
    public static Long timestampToLong(Timestamp timestamp) {
        return timestamp.getTime();
    }
}
