package de.mipnet.tourcollect.datamodel.datarepository;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import androidx.sqlite.db.SimpleSQLiteQuery;
import de.mipnet.tourcollect.datamodel.database.AppDatabase;
import de.mipnet.tourcollect.datamodel.database.DatabaseHelperDao;

public class DatabaseHelperRepository {
    private final String TAG = this.getClass().getSimpleName();
    private static DatabaseHelperDao databaseHelperDao;
    private static DatabaseHelperRepository instance;

    public static DatabaseHelperRepository getInstance(Application application) {
        if (instance == null) {
            instance = new DatabaseHelperRepository(application);
        }
        return instance;
    }

    private DatabaseHelperRepository(Application application) {
        AppDatabase db = AppDatabase.getAppDatabase(application);
        databaseHelperDao = db.databaseHelperDao();
    }

    public boolean createCheckpoint() {
        Object result = new Object();
        try {
            CreateCheckpointTask createCheckpointTask = new CreateCheckpointTask();
            result = createCheckpointTask.execute().get(10, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            Log.e(TAG, "createCheckpoint: Exception: ", e);
        }
        Log.d(TAG, "createCheckpoint: result:" + result);
        return result.equals(0);
    }

    private static class CreateCheckpointTask extends AsyncTask {

        @Override
        protected Object doInBackground(Object[] objects) {
            return databaseHelperDao.checkpoint(new SimpleSQLiteQuery("pragma wal_checkpoint(full)"));
        }
    }
}