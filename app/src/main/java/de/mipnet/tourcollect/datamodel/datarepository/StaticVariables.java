package de.mipnet.tourcollect.datamodel.datarepository;

final class StaticVariables {
    final static String GETLASTTOUR = "GETLASTTOUR";
    final static String GETTOURWITHID = "GETTOURWITHID";
    final static String GETALLTOURS = "GETALLTOURS";
}
