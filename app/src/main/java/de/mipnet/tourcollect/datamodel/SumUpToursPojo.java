package de.mipnet.tourcollect.datamodel;

import java.sql.Timestamp;

import static de.mipnet.tourcollect.StaticVariables.DAY;

public class SumUpToursPojo {
    public String timePeriodType;
    public Timestamp timestamp;
    public double distance;
    public long durationMoving;

    public SumUpToursPojo() {
        timePeriodType = DAY;
        timestamp = new Timestamp(0);
        distance = 0;
        durationMoving = 0;
    }
}
