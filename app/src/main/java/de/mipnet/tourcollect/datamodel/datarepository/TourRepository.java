package de.mipnet.tourcollect.datamodel.datarepository;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.database.AppDatabase;
import de.mipnet.tourcollect.datamodel.database.TourDao;

import static de.mipnet.tourcollect.StaticVariables.MOVINGTYPES;
import static de.mipnet.tourcollect.StaticVariables.SQL_ANY;
import static de.mipnet.tourcollect.datamodel.datarepository.StaticVariables.GETALLTOURS;
import static de.mipnet.tourcollect.datamodel.datarepository.StaticVariables.GETLASTTOUR;
import static de.mipnet.tourcollect.datamodel.datarepository.StaticVariables.GETTOURWITHID;

public class TourRepository {
    private final String TAG = this.getClass().getSimpleName();
    private final TourDao tourDao;
    private static TourRepository instance;

    public static TourRepository getInstance(Context context) {
        if (instance == null) {
            instance = new TourRepository(context);
        }
        return instance;
    }

    private TourRepository(Context context) {
        AppDatabase db = AppDatabase.getAppDatabase(context);
        tourDao = db.tourDao();
    }

    public long insertNewTour(Tour tour) {
        long rowId;
        try {
            insertNewTourAsyncTask insertNewTourAsyncTask = new insertNewTourAsyncTask(tourDao);
            rowId = insertNewTourAsyncTask.execute(tour).get(3, TimeUnit.SECONDS);
            return rowId;
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }
        Log.e(TAG, "insertNewTour: insert did not work.");
        return 0;
    }

    public void updateTour(Tour tour) {
        new updateTourAsyncTask(tourDao).execute(tour);
    }

    public void deleteTour(Tour tour) {
        if (tour.isRunningStatus()) {
            Log.w(TAG, "deleteTour: can't delete tour, since it is running.");
        } else {
            new deleteTourAsyncTask(tourDao).execute(tour);
        }
    }

    public DataSource.Factory getTours(String movingType) {
        return tourDao.getTours(movingType);
    }

    public LiveData<Tour> getLastTourLiveData() {
        return tourDao.getLastTourLiveData();
    }

    public Tour getLastTour() {
        getToursParam getToursParam = new getToursParam(GETLASTTOUR);
        try {
            getToursAsyncTask getToursAsyncTask = new getToursAsyncTask(tourDao);
            return getToursAsyncTask.execute(getToursParam).get(3, TimeUnit.SECONDS).get(0);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Log.w(TAG, "getLastTour: did not worked");
        return null;
    }

    public List<Tour> getAllTours() {
        getToursParam getToursParam = new getToursParam(GETALLTOURS);
        try {
            getToursAsyncTask getToursAsyncTask = new getToursAsyncTask(tourDao);
            return getToursAsyncTask.execute(getToursParam).get(10, TimeUnit.SECONDS);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Log.w(TAG, "getAllTours: did not worked");
        return null;
    }

    public Tour getTourWithId(long id) {
        getToursParam getToursParam = new getToursParam(GETTOURWITHID, SQL_ANY, id);
        try {
            getToursAsyncTask getToursAsyncTask = new getToursAsyncTask(tourDao);
            return getToursAsyncTask.execute(getToursParam).get(3, TimeUnit.SECONDS).get(0);
        } catch (TimeoutException | InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        Log.w(TAG, "getTourWithId: did not worked");
        return null;
    }

    private static class getToursParam {
        String taskExecution;
        String movingType;
        int limit;
        Timestamp begin, end;
        long id;

        getToursParam(String taskExecution) {
            this.taskExecution = taskExecution;
        }


        getToursParam(String taskExecution, String movingType, long id) {
            this.taskExecution = taskExecution;
            this.movingType = movingType;
            this.id =  id;
        }
    }

    private static class getToursAsyncTask extends AsyncTask<getToursParam, Void, List<Tour>> {

        private final TourDao tourDao;

        getToursAsyncTask(TourDao tourDao) {
            this.tourDao = tourDao;
        }

        @Override
        protected List<Tour> doInBackground(getToursParam... getToursParams) {
            if (getToursParams[0].taskExecution.equals(GETLASTTOUR)) {
                return tourDao.getLastTour();
            } else if (getToursParams[0].taskExecution.equals(GETALLTOURS)){
                return tourDao.getAllTours();
            } else {
                if (!Arrays.asList(MOVINGTYPES).contains(getToursParams[0].movingType)) {
                    Log.e("getToursAsyncTasks", "parameter not valid");
                    return null;
                } else {
                    return switch (getToursParams[0].taskExecution) {
                        case GETLASTTOUR -> tourDao.getLastTour();
                        case GETALLTOURS -> tourDao.getAllTours();
                        case GETTOURWITHID -> tourDao.getTourWithId(getToursParams[0].id);
                        default -> null;
                    };
                }
            }
        }
    }

    private static class insertNewTourAsyncTask extends AsyncTask<Tour, Void, Long> {

        private final String TAG = this.getClass().getSimpleName();

        private final TourDao asyncTaskDao;

        insertNewTourAsyncTask(TourDao tourDao) {
            asyncTaskDao = tourDao;
        }

        @Override
        protected Long doInBackground(Tour... tours) {
            long rowId = asyncTaskDao.insert(tours[0]);
            Log.i(TAG, "insertNewTourAsyncTask: insert Tour with id: " + rowId + " .");
            return rowId;
        }
    }

    private static class updateTourAsyncTask extends AsyncTask<Tour, Void, Void> {

        private TourDao asyncTaskDao;

        updateTourAsyncTask(TourDao tourDao) {
            asyncTaskDao = tourDao;
        }

        @Override
        protected Void doInBackground(Tour... tours) {
            asyncTaskDao.update(tours[0]);
            return null;
        }
    }

    private static class deleteTourAsyncTask extends AsyncTask<Tour, Void, Void> {

        private TourDao asyncTaskDao;

        deleteTourAsyncTask(TourDao tourDao) {
            asyncTaskDao = tourDao;
        }

        @Override
        protected Void doInBackground(Tour... tours) {
            asyncTaskDao.delete(tours[0]);
            return null;
        }
    }
}
