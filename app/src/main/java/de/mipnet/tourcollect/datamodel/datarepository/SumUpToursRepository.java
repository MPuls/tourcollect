package de.mipnet.tourcollect.datamodel.datarepository;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.LiveData;
import de.mipnet.tourcollect.datamodel.SumUpToursPojo;
import de.mipnet.tourcollect.datamodel.database.AppDatabase;
import de.mipnet.tourcollect.datamodel.database.SumUpToursDao;

public class SumUpToursRepository {
    private final SumUpToursDao sumUpToursDao;
    private static SumUpToursRepository instance;

    public static SumUpToursRepository getInstance(Application application) {
        if (instance == null) {
            instance = new SumUpToursRepository(application);
        }
        return instance;
    }

    private SumUpToursRepository(Application application) {
        AppDatabase db = AppDatabase.getAppDatabase(application);
        sumUpToursDao = db.sumUpToursDao();
    }

    public LiveData<List<SumUpToursPojo>> getSumUpToursByDay(String movingType) {
        return sumUpToursDao.getSumUpToursByDay(movingType);
    }

    public LiveData<List<SumUpToursPojo>> getSumUpToursByWeek(String movingType) {
        return sumUpToursDao.getSumUpToursByWeek(movingType);
    }

    public LiveData<List<SumUpToursPojo>> getSumUpToursByMonth(String movingType) {
        return sumUpToursDao.getSumUpToursByMonth(movingType);
    }

    public LiveData<List<SumUpToursPojo>> getSumUpToursByYear(String movingType) {
        return sumUpToursDao.getSumUpToursByYear(movingType);
    }
}
