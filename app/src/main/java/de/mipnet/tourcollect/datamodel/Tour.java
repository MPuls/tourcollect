package de.mipnet.tourcollect.datamodel;

import android.util.Log;

import java.sql.Timestamp;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static de.mipnet.tourcollect.StaticVariables.WALK;

/**
 * id | created | lastUpdate | movingType | runnningStatus | distance | durationTour | durationMoving
 */

@Entity(indices = @Index("created"))
public class Tour {
    @Ignore
    private final String TAG = this.getClass().getSimpleName();

    @PrimaryKey(autoGenerate = true)
    private long id;
    private Timestamp created;
    private Timestamp lastUpdate;
    private String movingType; // bike, foot, car, ...
    private boolean runningStatus;
    private double distance; // in meter
    private long durationTour; // in milliseconds
    private long durationMoving; // in milliseconds

    private double currentSpeed; //in m/s
    @Ignore
    private double averageSpeedMoving; // in meter/milliseconds
    @Ignore
    private double averageSpeedTour; // in meter/milliseconds

    public Tour() {
        created = new Timestamp(System.currentTimeMillis());
        lastUpdate = new Timestamp(System.currentTimeMillis());
        movingType = WALK;
        runningStatus = false;
        distance = 0;
        durationTour = 0;
        durationMoving = 0;
        currentSpeed = 0;
        averageSpeedMoving = 0;
        averageSpeedTour = 0;

        Log.i(TAG, "Tour: tour created with Tour().");
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public Timestamp getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Timestamp lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getMovingType() {
        return movingType;
    }

    public void setMovingType(String movingType) {
        this.movingType = movingType;
    }

    public boolean isRunningStatus() {
        return runningStatus;
    }

    public void setRunningStatus(boolean runningStatus) {
        this.runningStatus = runningStatus;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public long getDurationTour() {
        return durationTour;
    }

    public void setDurationTour(long durationTour) {
        this.durationTour = durationTour;
    }

    public long getDurationMoving() {
        return durationMoving;
    }

    public void setDurationMoving(long durationMoving) {
        this.durationMoving = durationMoving;
    }

    public double getCurrentSpeed() {
        return currentSpeed;
    }

    public void setCurrentSpeed(double currentSpeed) {
        this.currentSpeed = currentSpeed;
    }

    public double getAverageSpeedMoving() {
        if (distance == 0 || durationMoving == 0) {
            averageSpeedMoving = 0;
        } else {
            averageSpeedMoving = distance / durationMoving;
        }
        return averageSpeedMoving;
    }

    public static double getAverageSpeed(double distance, long duration) {
        if (distance != 0 && duration != 0) {
            return distance / duration;
        } else {
            return 0;
        }
    }

    public double getAverageSpeedTour() {
        if (distance == 0 || durationTour == 0) {
            averageSpeedTour = 0;
        } else {
            averageSpeedTour= distance / durationTour;
        }
        return averageSpeedTour;
    }
}
