package de.mipnet.tourcollect.datamodel.database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.paging.DataSource;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import de.mipnet.tourcollect.datamodel.Tour;

@Dao
public interface TourDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Tour tour);

    @Update
    void update(Tour tour);

    @Delete
    void delete(Tour tour);

    @Query("SELECT * FROM Tour WHERE id=(SELECT max(id) FROM Tour)")
    LiveData<Tour> getLastTourLiveData();

    @Query("SELECT * FROM Tour ORDER BY id DESC LIMIT 1")
    List<Tour> getLastTour();

    @Query("SELECT * FROM Tour WHERE id = :id")
    List<Tour> getTourWithId(long id);

    @Query("SELECT * FROM Tour ORDER BY id")
    List<Tour>getAllTours();

    @Query("SELECT * FROM Tour WHERE movingType LIKE :movingType ORDER BY id DESC")
    DataSource.Factory<Integer, Tour> getTours(String movingType);
}
