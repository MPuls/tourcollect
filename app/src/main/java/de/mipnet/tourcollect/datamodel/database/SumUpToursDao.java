package de.mipnet.tourcollect.datamodel.database;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;
import de.mipnet.tourcollect.datamodel.SumUpToursPojo;

@Dao
public interface SumUpToursDao {

    @Query("SELECT 'day' as timePeriodType, created as timestamp, SUM(durationMoving) as durationMoving, SUM(distance) as distance FROM Tour WHERE movingType like :movingType GROUP BY strftime('%Y-%m-%d', datetime(created/1000, 'unixepoch', 'localtime'))")
    LiveData<List<SumUpToursPojo>> getSumUpToursByDay(String movingType);

    @Query("SELECT 'week' as timePeriodType, created as timestamp, SUM(durationMoving) as durationMoving, SUM(distance) as distance FROM Tour WHERE movingType like :movingType GROUP BY strftime('%Y-%W', datetime(created/1000, 'unixepoch', 'localtime'))")
    LiveData<List<SumUpToursPojo>> getSumUpToursByWeek(String movingType);

    @Query("SELECT 'month' as timePeriodType, created as timestamp, SUM(durationMoving) as durationMoving, SUM(distance) as distance FROM Tour WHERE movingType like :movingType GROUP BY strftime('%Y-%m', datetime(created/1000, 'unixepoch', 'localtime'))")
    LiveData<List<SumUpToursPojo>> getSumUpToursByMonth(String movingType);

    @Query("SELECT 'year' as timePeriodType, created as timestamp, SUM(durationMoving) as durationMoving, SUM(distance) as distance FROM Tour WHERE movingType like :movingType GROUP BY strftime('%Y', datetime(created/1000, 'unixepoch', 'localtime'))")
    LiveData<List<SumUpToursPojo>> getSumUpToursByYear(String movingType);

}
