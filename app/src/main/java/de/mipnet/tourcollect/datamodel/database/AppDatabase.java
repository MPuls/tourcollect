package de.mipnet.tourcollect.datamodel.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;
import de.mipnet.tourcollect.datamodel.Tour;

import static android.content.ContentValues.TAG;
import static de.mipnet.tourcollect.StaticVariables.DATABASENAME;

@Database(
        entities = {Tour.class},
        version = 2
)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract TourDao tourDao();
    public abstract SumUpToursDao sumUpToursDao();
    public abstract DatabaseHelperDao databaseHelperDao();

    // Singleton Design pattern
    private static AppDatabase INSTANCE;

    public static AppDatabase getAppDatabase(Context context) {
        synchronized (AppDatabase.class) {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, DATABASENAME)
                        .addMigrations(MIGRATION_1_2)
                        .build();
                Log.i(TAG, "getAppDatabase: Database " + DATABASENAME + " created");
            }
        }
        return INSTANCE;
    }

    // Manual migrations
    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("ALTER TABLE Tour ADD COLUMN currentSpeed REAL NOT NULL DEFAULT 0");
        }
    };
}
