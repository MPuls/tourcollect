package de.mipnet.tourcollect;


import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;
import androidx.preference.PreferenceManager;
import de.mipnet.tourcollect.about.AboutFragment;
import de.mipnet.tourcollect.currenttour.view.CurrentTourFragment;
import de.mipnet.tourcollect.lasttours.view.LastToursFragment;
import de.mipnet.tourcollect.preferences.SettingsFragment;
import de.mipnet.tourcollect.tourstats.StatsTabsFragment;

import static de.mipnet.tourcollect.StaticVariables.BIKE;
import static de.mipnet.tourcollect.StaticVariables.CAR;
import static de.mipnet.tourcollect.StaticVariables.MOVING_TYPE;
import static de.mipnet.tourcollect.StaticVariables.RUN;
import static de.mipnet.tourcollect.StaticVariables.WALK;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FinishAllActivities {

    final String TAG = this.getClass().getSimpleName();

    CurrentTourFragment currentTourFragment = new CurrentTourFragment();
    LastToursFragment lastToursFragment = new LastToursFragment();
    StatsTabsFragment walkToursFragment = new StatsTabsFragment();
    StatsTabsFragment runToursFragment = new StatsTabsFragment();
    StatsTabsFragment bikeToursFragment = new StatsTabsFragment();
    StatsTabsFragment carToursFragment = new StatsTabsFragment();
    SettingsFragment settingsFragment = new SettingsFragment();
    AboutFragment aboutFragment = new AboutFragment();

    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MainActivityController mainActivityController = MainActivityController.getInstance();
        mainActivityController.setMainActivity(this);

        String themePref = PreferenceManager.getDefaultSharedPreferences(this)
                .getString(getResources().getString(R.string.prefs_theme_key), "EMPTY");
        Log.d(TAG, "onCreate: themePref: " + themePref);
        switch (themePref) {
            case "BLUE" -> setTheme(R.style.AppTheme_BLUE);
            case "GREY" -> setTheme(R.style.AppTheme_GREY);
            case "ORANGE" -> setTheme(R.style.AppTheme_ORANGE);
            case "SLATE" -> setTheme(R.style.AppTheme_SLATE);
            default -> setTheme(R.style.AppTheme_GREEN);
        }

        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
             this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        var onBackPressed = new OnBackPressedCallback(true) {

            @Override
            public void handleOnBackPressed() {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    drawer.closeDrawer(GravityCompat.START);
                }
            }
        };
        this.getOnBackPressedDispatcher().addCallback(this, onBackPressed);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (savedInstanceState == null) {
            transaction.add(R.id.fragment_container, currentTourFragment);
            transaction.commit();
            toolbar.setTitle(R.string.currentTour);
        }
    }


    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("alreadyRunning", true);
    }


    public void finishAllActivities() {
        Log.d(TAG, "finish all activities");
        finishAffinity();
        Runtime.getRuntime().exit(0);
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Bundle bundle = new Bundle();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();

        if (id == R.id.nav_current_tour) {
            transaction.replace(R.id.fragment_container, currentTourFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.currentTour);
        } else if (id == R.id.nav_last_tours) {
            transaction.replace(R.id.fragment_container, lastToursFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.last_tours);
        } else if (id == R.id.nav_walk) {
            bundle.putString(MOVING_TYPE, WALK);
            walkToursFragment.setArguments(bundle);
            transaction.replace(R.id.fragment_container, walkToursFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.walking);
        } else if (id == R.id.nav_run) {
            bundle.putString(MOVING_TYPE, RUN);
            runToursFragment.setArguments(bundle);
            transaction.replace(R.id.fragment_container, runToursFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.running);
        } else if (id == R.id.nav_bike) {
            bundle.putString(MOVING_TYPE, BIKE);
            bikeToursFragment.setArguments(bundle);
            transaction.replace(R.id.fragment_container, bikeToursFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.bike);
        } else if (id == R.id.nav_car) {
            bundle.putString(MOVING_TYPE, CAR);
            carToursFragment.setArguments(bundle);
            transaction.replace(R.id.fragment_container, carToursFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.car);
        } else if (id == R.id.nav_settings) {
            transaction.replace(R.id.fragment_container, settingsFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.settings);
        } else if (id == R.id.nav_about) {
            transaction.replace(R.id.fragment_container, aboutFragment);
            transaction.addToBackStack(null);
            transaction.commit();
            toolbar.setTitle(R.string.about);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
}
