package de.mipnet.tourcollect.preferences;

import android.text.InputType;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.preference.EditTextPreference;

class OnBindEditTextNumberListener implements EditTextPreference.OnBindEditTextListener {
    @Override
    public void onBindEditText(@NonNull EditText editText) {
        editText.setInputType(InputType.TYPE_CLASS_NUMBER);
    }
}
