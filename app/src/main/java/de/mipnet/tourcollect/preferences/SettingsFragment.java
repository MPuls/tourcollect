package de.mipnet.tourcollect.preferences;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import java.sql.Timestamp;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.preference.EditTextPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceManager;
import androidx.preference.SwitchPreferenceCompat;
import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.PermissionHandler;
import de.mipnet.tourcollect.R;

public class SettingsFragment extends PreferenceFragmentCompat {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onCreatePreferences(@Nullable Bundle savedInstanceState, String s) {
        addPreferencesFromResource(R.xml.preferences);

        Preference themePref = findPreference(getString(R.string.prefs_theme_key));
        assert themePref != null;
        themePref.setOnPreferenceChangeListener((preference, newValue) -> {
            PreferenceManager.getDefaultSharedPreferences(requireContext())
                    .edit().putString(getResources().getString(R.string.prefs_theme_key), (String) newValue)
                    .apply();
            requireActivity().recreate();
            return true;
        });


        SwitchPreferenceCompat globalFontSizeSwitch = findPreference(getResources().getString(R.string.prefs_change_global_font_size_key));
        assert globalFontSizeSwitch != null;
        globalFontSizeSwitch.setOnPreferenceChangeListener((preference, newValue) -> {
            // TODO: Maybe remove permission after changing the setting.
            if ((boolean)newValue && PermissionHandler.isWriteSettingsPermissionGranted(getActivity())) {
                return true;
            } else return !(boolean) newValue;
        });

        EditTextPreference speedLimitMovingEditText = getPreferenceManager().findPreference(getResources().getString(R.string.prefs_speedLimitMoving_key));
        EditTextPreference speedLimitRunEditText = getPreferenceManager().findPreference(getResources().getString(R.string.prefs_speedLimitRun_key));
        EditTextPreference speedLimitBikeEditText = getPreferenceManager().findPreference(getResources().getString(R.string.prefs_speedLimitBike_key));
        EditTextPreference speedLimitCarEditText = getPreferenceManager().findPreference(getResources().getString(R.string.prefs_speedLimitCar_key));
        assert speedLimitMovingEditText != null;
        assert speedLimitRunEditText != null;
        assert speedLimitBikeEditText != null;
        assert speedLimitCarEditText != null;
        speedLimitMovingEditText.setOnBindEditTextListener(new OnBindEditTextNumberListener());
        speedLimitRunEditText.setOnBindEditTextListener(new OnBindEditTextNumberListener());
        speedLimitBikeEditText.setOnBindEditTextListener(new OnBindEditTextNumberListener());
        speedLimitCarEditText.setOnBindEditTextListener(new OnBindEditTextNumberListener());

        ActivityResultLauncher<Intent> csvExportResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        CsvExport csvExport = new CsvExport(requireActivity().getApplication());
                        csvExport.export(uri);
                    }
                });

        Preference csvExportBtn = findPreference(getString(R.string.prefs_csvExport_key));
        assert csvExportBtn != null;
        csvExportBtn.setOnPreferenceClickListener(preference -> {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            String timeOfExport = FormatHelper.formatTimestampToExportFormat(now);
            String filename = "tourcollect-export_" + timeOfExport + ".csv";

            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("csv/plain");
            intent.putExtra(Intent.EXTRA_TITLE, filename);
            csvExportResultLauncher.launch(intent);
            return true;
        });


        ActivityResultLauncher<Intent> dBExportResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if (result.getResultCode() == Activity.RESULT_OK) {
                        Intent data = result.getData();
                        assert data != null;
                        Uri uri = data.getData();
                        DBImportExport dbImportExport = new DBImportExport(requireActivity().getApplication());
                        dbImportExport.exportDB(uri);
                    }
                });

        Preference dbExportBtn = findPreference(getString(R.string.prefs_dbExport_key));
        assert dbExportBtn != null;
        dbExportBtn.setOnPreferenceClickListener(preference -> {
            Timestamp now = new Timestamp(System.currentTimeMillis());
            String timeOfExport = FormatHelper.formatTimestampToExportFormat(now);
            String filename = "tourcollect-export_" + timeOfExport + ".db";

            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("db/plain");
            intent.putExtra(Intent.EXTRA_TITLE, filename);
            dBExportResultLauncher.launch(intent);
            return true;
        });

        Preference dbImportBtn = findPreference(getString(R.string.prefs_dbImport_key));
        assert dbImportBtn != null;
        dbImportBtn.setOnPreferenceClickListener(preference -> {
            ImportDialogFragment importDialogFragment = new ImportDialogFragment();
            importDialogFragment.show(requireActivity().getSupportFragmentManager(), "importDialog");
            return true;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        // Update globalFontSizeSwitch with the saved value which might be altered via the currentTour toolbar settings
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(requireContext());
        boolean isGlobalFontSizeSwitchOn = sharedPreferences.getBoolean(getResources().getString(R.string.prefs_change_global_font_size_key), false);
        SwitchPreferenceCompat globalFontSizeSwitch = findPreference(getString(R.string.prefs_change_global_font_size_key));
        assert globalFontSizeSwitch != null;
        globalFontSizeSwitch.setChecked(isGlobalFontSizeSwitchOn);
        Log.d(TAG, "onCreate: isGlobalFontSizeSwitchOn: " + isGlobalFontSizeSwitchOn);
    }
}
