package de.mipnet.tourcollect.preferences;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import de.mipnet.tourcollect.MainActivityController;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.helper.FileHelper;

import static de.mipnet.tourcollect.StaticVariables.DB_MIME_TYPE;

public class ImportDialogFragment extends DialogFragment {

    private final String TAG = this.getClass().getSimpleName();
    // private static int REQUEST_SELECT_IMPORT_FILE = 42;

    private EditText etFilePath;
    TextView btnImport;

    @Override
    public void onCancel(@NonNull DialogInterface dialog) {
        super.onCancel(dialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.import_dialog, container, false);
    }

    ActivityResultLauncher<Intent> dBImportResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    var context = getContext();
                    Intent data = result.getData();

                    if (context != null && data != null) {
                        Uri importFileUri = data.getData();

                        Log.d(TAG, "onActivityResult: file selected");

                        String fileName = FileHelper.getFileName(importFileUri, context);
                        etFilePath.setText(fileName);

                        String mimeType = FileHelper.getMimeType(importFileUri, context);
                        Log.d(TAG, "onActivityResult: mime-type = [" + mimeType + "]");

                        if (mimeType != null && mimeType.equals(DB_MIME_TYPE)) {
                            TypedValue typedValue = new TypedValue();
                            requireActivity().getTheme().resolveAttribute(R.attr.colorPrimary, typedValue, true);
                            btnImport.setTextColor(typedValue.data);
                            btnImport.setEnabled(true);

                            btnImport.setOnClickListener(v -> {
                                var application = requireActivity().getApplication();
                                DBImportExport dbImportExport = new DBImportExport(application);
                                try {
                                    if(dbImportExport.importDB(importFileUri)) {
                                        Toast.makeText(application, getString(R.string.import_successfull), Toast.LENGTH_LONG).show();
                                        MainActivityController mainActivityController = MainActivityController.getInstance();
                                        mainActivityController.triggerFinishAllActivities();
                                    }
                                } catch (Error | Exception  e) {
                                    Log.e(TAG, "importDB: e: " + e.getMessage());
                                    Toast.makeText(application, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    } else {
                        Log.e(TAG, "Either context or data is null");
                    }
                }
            });

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView btnSelectFile = view.findViewById(R.id.dlgImport_selectFile);
        TextView btnCancel = view.findViewById(R.id.dlgImport_cancel);
        btnImport = view.findViewById(R.id.dlgImport_import);
        etFilePath = view.findViewById(R.id.dlgImport_filePath);

        btnImport.setTextColor(ContextCompat.getColor(requireContext(),R.color.grey_88));

        btnCancel.setOnClickListener(v -> dismiss());

        btnSelectFile.setOnClickListener(v -> {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setTypeAndNormalize("application/*");
                dBImportResultLauncher.launch(intent);
            }
        );

    }
}
