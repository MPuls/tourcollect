package de.mipnet.tourcollect.preferences;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Objects;

import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.datarepository.DatabaseHelperRepository;
import de.mipnet.tourcollect.helper.FileHelper;

import static de.mipnet.tourcollect.StaticVariables.DATABASENAME;



class DBImportExport {
    private final String TAG = this.getClass().getSimpleName();
    private final Application application;
    private final DatabaseHelperRepository databaseHelper;

    private final File currentDBFile;
    private final File currentDBPath;
    private final Resources resources;

    DBImportExport(Application application) {
        super();
        Log.i(TAG, "DBImportExport: constructor");
        this.application = application;
        this.resources = application.getResources();
        currentDBFile = application.getApplicationContext().getDatabasePath(DATABASENAME);
        currentDBPath = new File(Objects.requireNonNull(currentDBFile.getParent()));

        this.databaseHelper = DatabaseHelperRepository.getInstance(application);
    }

    void exportDB(Uri uri) {
        Log.i(TAG, "exportDB: init");
        Context context = application.getApplicationContext();

        try {
            if (!databaseHelper.createCheckpoint()) throw new Error("Checkpoint for Database could not be set.");

            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "w");
            if( pfd != null ) {
                final var os = new FileOutputStream(pfd.getFileDescriptor());
                final var is = new FileInputStream(currentDBFile);
                final FileChannel destination = os.getChannel();
                final FileChannel source = is.getChannel();
                destination.transferFrom(source, 0, source.size());
                destination.close();
                source.close();
                is.close();
                os.close();
                Log.d(TAG, "exportDB: currentDBFile: [" + currentDBFile + "], backupDB: [" + FileHelper.getFileName(uri, context) + "]");
                pfd.close();
            }

            Toast.makeText(application, resources.getString(R.string.was_created), Toast.LENGTH_LONG).show();

        } catch (Error error) {
            Log.e(TAG, "exportDB: error: " + error.getMessage());
            Toast.makeText(application, "Error: " + error.getMessage(), Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.e(TAG, "exportDB: exception: ", e);
            Toast.makeText(application, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        Log.i(TAG, "exportDB: end");
    }

    boolean importDB(Uri importFileUri) throws Error, IOException, NullPointerException {
        Log.i(TAG, "import: init");
        Log.d(TAG, "importDB() called with: importFileUri = [" + importFileUri + "]");
        final File importDBFile = new File(importFileUri.getPath());

        if (!currentDBPath.canWrite()) throw new Error("Cannot write on storage.");

        final File currentDBShmFile = new File(currentDBPath, DATABASENAME + "-shm");
        final File currentDBWalFile = new File(currentDBPath, DATABASENAME + "-wal");
        Log.d(TAG, "importDB: currentDBFile: [" + currentDBFile + "], currentDBShmFile: [" + currentDBShmFile + "], currentDBWalFile: [" + currentDBWalFile + "],  importDBFile: [" + importDBFile + "]");

        if (currentDBShmFile.exists()) {
            if (!currentDBShmFile.delete()) throw new Error("DB-SHM file could not be deleted.");
        }
        if (currentDBWalFile.exists()) {
            if (!currentDBWalFile.delete()) throw new Error("DB-WAL file could not be deleted.");
        }

        ParcelFileDescriptor parcelImportFileDescriptor = application.getContentResolver().openFileDescriptor(importFileUri, "r");
        final var is = new FileInputStream(parcelImportFileDescriptor.getFileDescriptor());
        final var os = new FileOutputStream(currentDBFile);
        final FileChannel source = is.getChannel();
        final FileChannel destination = os.getChannel();
        destination.transferFrom(source, 0, source.size());
        destination.close();
        source.close();
        os.close();
        is.close();
        parcelImportFileDescriptor.close();

        Log.i(TAG, "import: end");
        return true;
    }
}
