package de.mipnet.tourcollect.preferences;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.widget.Toast;

import java.io.OutputStream;
import java.util.List;

import de.mipnet.tourcollect.FormatHelper;
import de.mipnet.tourcollect.R;
import de.mipnet.tourcollect.datamodel.Tour;
import de.mipnet.tourcollect.datamodel.datarepository.TourRepository;

public class CsvExport {
    final String TAG = this.getClass().getSimpleName();
    TourRepository tourRepository;
    Application application;
    final Resources resources;

    public CsvExport(Application application) {
        super();
        this.application = application;
        this.resources = application.getResources();
        this.tourRepository = TourRepository.getInstance(application);
        Log.i(TAG, "CsvExport: constructor");
    }

    private List<Tour> getAllTours() {
        return tourRepository.getAllTours();
    }

    private String[] getCsvSanitizedTour(Tour tour) {
        return new String[]{
                tour.getCreated().toString(),
                tour.getLastUpdate().toString(),
                "\"" + tour.getMovingType() + "\"",
                FormatHelper.formatDistance(tour.getDistance()),
                FormatHelper.formatDurationSec(tour.getDurationTour()),
                FormatHelper.formatAverageSpeed(tour.getAverageSpeedTour()),
                FormatHelper.formatDurationSec(tour.getDurationMoving()),
                FormatHelper.formatAverageSpeed(tour.getAverageSpeedMoving())
        };
    }

    public void export(Uri uri) {
        Log.i(TAG, "export: init");
        Context context = application.getApplicationContext();

        String[] header = {"\"created\"", "\"last_update\"", "\"moving_type\"", "\"distance\"", "\"tour_duration\"", "\"tour_average_speed\"", "\"moving_duration\"", "\"moving_average_speed\""};
        List<Tour> tours = getAllTours();

        String csvStringData = String.join(";", header) + "\n";
        for (Tour tour : tours) {
            String[] tourArray = getCsvSanitizedTour(tour);
            String csvTour = String.join(";", tourArray);
            Log.d(TAG, "export: " + csvTour);
            csvStringData = csvStringData.concat(csvTour + "\n");
        }

        try {
            ParcelFileDescriptor pfd = context.getContentResolver().openFileDescriptor(uri, "w");
            if( pfd != null ) {
                OutputStream os = context.getContentResolver().openOutputStream(uri);
                if( os != null ) {
                    os.write(csvStringData.getBytes());
                    os.close();
                    Toast.makeText(application, resources.getString(R.string.was_created), Toast.LENGTH_LONG).show();
                }
                pfd.close();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            Log.e(TAG, "export: Error", e);
            Toast.makeText(application, resources.getString(R.string.could_not_be_created), Toast.LENGTH_LONG).show();
        }

        Log.i(TAG, "export: end");
    }
}
