package de.mipnet.tourcollect.currenttour;

import android.content.Context;
import android.location.LocationManager;
import android.widget.TextView;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import java.util.Locale;

import androidx.test.core.app.ActivityScenario;
import androidx.test.espresso.PerformException;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.platform.app.InstrumentationRegistry;
import de.mipnet.tourcollect.MainActivity;
import de.mipnet.tourcollect.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withTagValue;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CurrentTourTests {

    private static MockLocationProvider mockLocationProvider;
    private final String TAG = this.getClass().getSimpleName();

    @ClassRule
    public static final ForceLocaleRule localeTestRule = new ForceLocaleRule(Locale.UK);

    @Rule
    public ActivityScenarioRule<MainActivity> rule =
            new ActivityScenarioRule<>(MainActivity.class);

    @BeforeClass
    public static void testSuiteSetup() {
        Context context = InstrumentationRegistry.getInstrumentation().getTargetContext();
        mockLocationProvider = new MockLocationProvider(LocationManager.GPS_PROVIDER, context);
    }

    @Before
    public void testCaseSetup() {
        mockLocationProvider.reset();
        // check if the new tour button is clickable (means, that no current tour is running, which happens sometimes, when the app crashes or was closed by failed tests)
        try {
            newTour();
        } catch (PerformException e) {
            pauseTour();
            newTour();
        }

        // start a new tour in order to have a clear setup state
        newTour();
    }

    @AfterClass
    public static void testSuiteAfter() {
        mockLocationProvider.shutdown();
    }

    @Test
    public void show_pause_button_while_running() {
        startTour();
        checkShowingPauseButton();
        pauseTour();
    }

    @Test
    public void when_speed_is_0() throws InterruptedException {
        startTour();
        mockLocationProvider.go(0, 5);
        pauseTour();

        checkValueDistance(0);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 5);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 0);
    }

    @Test
    public void when_speed_is_below_limit() throws InterruptedException {
        startTour();
        mockLocationProvider.go(3, 5);
        pauseTour();

        checkValueDistance(0);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 5);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 0);
    }

    @Test
    public void stop_and_go_scenario() throws InterruptedException {
        double testDistance;
        startTour();

        mockLocationProvider.go(0, 5);
        testDistance = 0;
        checkValueDistance(testDistance);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 5);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 0);
        checkShowingPauseButton();

        mockLocationProvider.go(20, 5);
        testDistance = mockLocationProvider.getDistance();
        checkValueDistance(testDistance);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 10);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 5);
        checkShowingPauseButton();

        mockLocationProvider.go(3, 5);
        checkValueDistance(testDistance);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 15);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 5);
        checkShowingPauseButton();

        mockLocationProvider.go(15, 5);
        testDistance = mockLocationProvider.getDistance();
        checkValueDistance(testDistance);
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, 20);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, 10);
        checkShowingPauseButton();
    }

    @Test
    public void stop_values_after_pause() throws InterruptedException {

        float speed = 200f; // 200 km/h
        int duration = 3; // 3 seconds

        // Before
        startTour();
        mockLocationProvider.go(speed, duration);
        pauseTour();

        checkValueDistance(mockLocationProvider.getDistance());
        checkValueDuration(R.id.currentTour_durationTourSecondsValue_tv, duration);
        checkValueDuration(R.id.currentTour_durationMovingSecondsValue_tv, duration);
    }

    @Test
    public void zero_values_and_buttons_after_new_tour_created() throws InterruptedException {

        // Before
        startTour();
        mockLocationProvider.go(200, 3);
        pauseTour();
        checkIfValuesAreNotZero();

        // Test
        newTour();

        checkShowingPlayButton();
        checkShowingNewTourButton();
        onView(withId(R.id.currentTour_currentSpeed_tv)).check(matches(withText("0")));
        onView(withId(R.id.currentTour_distance_tv)).check(matches(withText("0.0")));
        onView(withId(R.id.currentTour_durationTourHoursValue_tv)).check(matches(withText("0")));
        onView(withId(R.id.currentTour_durationTourMinutesValue_tv)).check(matches(withText("00")));
        onView(withId(R.id.currentTour_durationTourSecondsValue_tv)).check(matches(withText("00")));
        onView(withId(R.id.currentTour_durationMovingHoursValue_tv)).check(matches(withText("0")));
        onView(withId(R.id.currentTour_durationMovingMinutesValue_tv)).check(matches(withText("00")));
        onView(withId(R.id.currentTour_durationMovingSecondsValue_tv)).check(matches(withText("00")));
        onView(withId(R.id.currentTour_averageSpeedTour_tv)).check(matches(withText("0.0")));
        onView(withId(R.id.currentTour_averageSpeedMoving_tv)).check(matches(withText("0.0")));

    }

    @Test
    public void show_play_button_after_pause() throws InterruptedException {
        // Before
        startTour();
        mockLocationProvider.go(200, 3);
        pauseTour();

        // Test
        checkShowingPlayButton();
    }

    private void startTour() {
        onView(withTagValue(is(R.id.play))).perform(click());
    }

    private void pauseTour() {
        onView(withTagValue(is(R.id.pause))).perform(click());
    }

    private void newTour() {
        onView(withId(R.id.currentTour_NewTour_btn)).perform(click());
    }

    private double getBoundary(double x) {
        return Math.pow(10, 1/Math.pow(x, 0.25)) - 1;
    }

    /**
     * @param distance in meter
     */
    private void checkValueDistance(double distance) {
        var distanceInKm = distance / 1000;
        double bottomBoundary = distanceInKm - getBoundary(distanceInKm * 10)/10;
        double upperBoundary = distanceInKm + getBoundary(distanceInKm * 10)/10;
        ActivityScenario<MainActivity> scenario = rule.getScenario();
        scenario.onActivity(activity -> {
            TextView tvDistance = activity.findViewById(R.id.currentTour_distance_tv);
            var distanceUnderTest = Double.parseDouble(tvDistance.getText().toString());
            assertTrue("distance " + distanceUnderTest + " is lower than bottom-boundary " + bottomBoundary, distanceUnderTest >= bottomBoundary);
            assertTrue("distance " + distanceUnderTest + " is higher than upper-boundary " + upperBoundary, distanceUnderTest <= upperBoundary);
        });
    }

    private void checkValueDuration(int viewId, long durationInSec) {
        double bottomBoundary = durationInSec - getBoundary(durationInSec);
        double upperBoundary = durationInSec + getBoundary(durationInSec);
        ActivityScenario<MainActivity> scenario = rule.getScenario();
        scenario.onActivity(activity -> {
            TextView tvDurationSeconds = activity.findViewById(viewId);
            long secondsUnderTest = Long.parseLong(tvDurationSeconds.getText().toString());
            assertTrue("seconds under test: " + secondsUnderTest + " is lower than the bottom-boundary " + bottomBoundary + " with target seconds: " + durationInSec, secondsUnderTest >= bottomBoundary);
            assertTrue("seconds under test: " + secondsUnderTest + " is higher than the upper-boundary " + upperBoundary + " with target seconds: " + durationInSec, secondsUnderTest <= upperBoundary);
        });
    }

    private void checkIfValuesAreNotZero() {
        onView(withId(R.id.currentTour_currentSpeed_tv)).check(matches(not(withText("0"))));
        onView(withId(R.id.currentTour_distance_tv)).check(matches(not(withText("0.0"))));
        onView(withId(R.id.currentTour_durationTourSecondsValue_tv)).check(matches(not(withText("00"))));
        onView(withId(R.id.currentTour_durationMovingSecondsValue_tv)).check(matches(not(withText("00"))));
        onView(withId(R.id.currentTour_averageSpeedTour_tv)).check(matches(not(withText("0.0"))));
        onView(withId(R.id.currentTour_averageSpeedMoving_tv)).check(matches(not(withText("0.0"))));
    }

    private void checkShowingPauseButton() {
        onView(withTagValue(is(R.id.pause))).check(matches(isDisplayed()));
    }

    private void checkShowingPlayButton() {
        onView(withTagValue(is(R.id.play))).check(matches(isDisplayed()));
    }

    private void checkShowingNewTourButton() {
        onView(withId(R.id.currentTour_NewTour_btn)).check(matches(isDisplayed()));
    }
}
