package de.mipnet.tourcollect.currenttour;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

/**
 * <a href="https://mobiarch.wordpress.com/2012/07/17/testing-with-mock-location-data-in-android/">testing-with-mock-location-data-in-android</a>
 */

class MockLocationProvider {

    private final String TAG = this.getClass().getSimpleName();
    private final String providerName;
    private final Context ctx;
    private double lastDistance;

    MockLocationProvider(String name, Context ctx) {
        this.providerName = name;
        this.ctx = ctx;
        this.lastDistance = 0;

        LocationManager lm = (LocationManager) ctx.getSystemService(Context.LOCATION_SERVICE);
        lm.addTestProvider(providerName, false, false, false, false, false,
                true, true, 1, 1);
        lm.setTestProviderEnabled(providerName, true);
    }

    void shutdown() {
        LocationManager lm = (LocationManager) ctx.getSystemService(
                Context.LOCATION_SERVICE);
        lm.removeTestProvider(providerName);
    }

    void reset() {
        lastDistance = 0;
    }

    /**
     * @return distance in meter
     */
    double getDistance() { return lastDistance; }

    /**
     *
     * @param speedInKmH in km/h
     * @param durationInSec in seconds
     * @throws InterruptedException because of interruption of Thread.sleep()
     */
    void go(float speedInKmH, long durationInSec) throws InterruptedException {
        //Log.d(TAG, "go: pushLocation:  new lon: " + newLon);
        pushLocation(0, getLon(lastDistance), speedInKmH);
        double distancePerSec = getDistance(durationInSec, speedInKmH) / durationInSec;
        for (int t = 1; t <= Math.round((float)durationInSec); t++) {
            //noinspection BusyWait
            Thread.sleep(1000);
            double newDistance = lastDistance + distancePerSec;
            Log.i(TAG, "go: newDistance: " + newDistance);
            double newLon = getLon(newDistance);
            pushLocation(0, newLon, speedInKmH);
            lastDistance = newDistance;
        }
    }


    /**
     *
     * @param lat Latitude value
     * @param lon Longitude value
     * @param speed in km/h
     */
    private void pushLocation(@SuppressWarnings("SameParameterValue") double lat, double lon, float speed) {
        LocationManager lm = (LocationManager) ctx.getSystemService(
                Context.LOCATION_SERVICE);

        Location mockLocation = new Location(providerName);
        mockLocation.setLatitude(lat);
        mockLocation.setLongitude(lon);
        mockLocation.setSpeed(kmPerHourInMeterPerSecond(speed));
        mockLocation.setAltitude(0);
        mockLocation.setTime(System.currentTimeMillis());
        mockLocation.setElapsedRealtimeNanos(System.nanoTime());
        mockLocation.setAccuracy(5);
        lm.setTestProviderLocation(providerName, mockLocation);
        Log.d(TAG, "pushLocation: location: " + mockLocation);
    }

    /**
     *
     * @param distance in meter
     * @return Longitude value if latitude is always zero
     */
    private double getLon(double distance) {
        double R = 6371d*1000;
        return distance/R*180/Math.PI;
    }

    /**
     *
     * @param duration in seconds
     * @param speed in km/h
     * @return distance in meter
     */
    private double getDistance(long duration, float speed) {
        return duration*kmPerHourInMeterPerSecond(speed);
    }

    private float kmPerHourInMeterPerSecond(float kmh) {
        return kmh/3.6f;
    }
}
