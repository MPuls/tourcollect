package de.mipnet.tourcollect.view;

import org.junit.Test;

import java.util.Map;

import de.mipnet.tourcollect.FormatHelper;

import static de.mipnet.tourcollect.StaticVariables.MINUTE;
import static de.mipnet.tourcollect.StaticVariables.SECOND;
import static junit.framework.Assert.assertEquals;

public class FormatHelperTest {
    

    @Test
    public void formatDuration_59s() {
        long duration = 59*1000; // milliseconds

        Map<String, String> formatDuration = FormatHelper.formatDuration(duration);
        String seconds = formatDuration.get(SECOND);
        String minutes = formatDuration.get(MINUTE);

        assertEquals("After running 59 seconds, the shown value for seconds should be 59. But it is " + seconds + ".", "59", seconds);
        assertEquals("After running 59 seconds, the shown value for minutes should be 00. But it is " + minutes + ".", "00", minutes);
    }

    @Test
    public void formatDuration_60s() {
        long duration = 60*1000; // milliseconds

        Map<String, String> formatDuration = FormatHelper.formatDuration(duration);
        String seconds = formatDuration.get(SECOND);
        String minutes = formatDuration.get(MINUTE);

        assertEquals("After running 60 seconds, the shown value for seconds should be 00. But it is " + seconds + ".", "00", seconds);
        assertEquals("After running 60 seconds, the shown value for minutes should be 01. But it is " + minutes + ".", "01", minutes);
    }

    @Test
    public void formatDuration_61s() {
        long duration = 61*1000; // milliseconds

        Map<String, String> formatDuration = FormatHelper.formatDuration(duration);
        String seconds = formatDuration.get(SECOND);
        String minutes = formatDuration.get(MINUTE);

        assertEquals("After running 61 seconds, the shown value for seconds should be 01. But it is " + seconds + ".", "01", seconds);
        assertEquals("After running 61 seconds, the shown value for minutes should be 01. But it is " + minutes + ".", "01", minutes);
    }

}
