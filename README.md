# TourCollect

![TourCollect Logo](app/src/main/res/mipmap-hdpi/ic_launcher.png)

TourCollect is a [GPL v3](LICENSE) Android App with the main purpose to function as a replacement for a bike computer.

Additionally, it works for tours by hiking, jogging or driving a car.

## Main features

* Shows the current running tour.
* Calculates the duration and average speed of tour for the whole time of the tour and only for the time while in motion.
* Can increase Android's overall font-size while the tour is running.
* Decides autonomously the motion type of the tour (walking / hiking, jogging, bike or car) by the average speed.
  The speed limits can be adjusted by the user.
* Show a list of last tours in which one can filter for the tour's motion type.
* Showing statistics for each motion type by day, week, month and year.
* Export to CSV.

## UI

![Current tour](doc/ui/current-tour.jpg)

![Menu](doc/ui/main-menu.jpg)

![Last tours](doc/ui/last-tours.jpg)

![Statistics](doc/ui/statistics-bike-month.jpg)
