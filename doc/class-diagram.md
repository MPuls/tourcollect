# Class diagrams

## CurrentTour

```mermaid
classDiagram
    
    namespace UiLayers {
        class MainActivity
        class CurrentTourFragment
        class CurrentTourVM {
            -TourRepository tourRepository
            -LiveData~Tour~ currentTourLiveData
            -StoreCurrentTourLifeCycle storeCurrentTourLifeCycle
            +CurrentTourVM(Application application)
            +start()
            +pause()
            +newCurrentTour()
            +getCurrentTourLiveData(): LiveData~Tour~
        }
    }

    namespace DataLayer {
        class RoomDatabase
        class AppDatabase{
            +getAppDatabase(Context context)$ AppDatabase
            +tourDao()* TourDao
            +sumUpToursDao()* SumUpRoursDao
            +databaseHelperDao()* DatabaseHelperDao
        }
        class TourDao {
            <<interface>>
            insert(Tour tour)
            update(Tour tour)
            delete(Tour tour)
            getLastTour() List~Tour~
            getTourWithId(long id) List~Tour~
            getAllTours() List~Tour~
            getTours(String movingType) PaginatedList~Tour~
        }
        class TourRepository {
            -TourDao tourDao
            +getInstance(Context context)$ TourRepository
            +insertNewTour(Tour tour)
            +updateTour(Tour tour)
            +deleteTour(Tour tour)
            +getTours(String movingType) PaginatedList~Tour~
            +getLastTour() Tour
            +getAllTours() List~Tour~
            +getTourWithId(long id) Tour
        }
    }
    RoomDatabase <|-- AppDatabase
    AppDatabase <-- TourRepository

    namespace Model {
        class Tour {
            -long id
            -Timestamp created
            -Timestamp lastUpdate
            -String movingType
            -boolean runningStatus
            -double distance
            -long durationTour
            -long durationMoving
            -double currentSpeed
            -double averageSpeedMoving
            -double averageSpeedTour
            +Tour()
            +setter
            +getter
            
        }
    }

    namespace Services {
        class LocationService {
            -Context applicationContext
            -StoreOnLocationChange storeOnLocationChange
            +onCreate()
            +onStartCommand(Intent intent)
            +onDestroy()
            +onLocationChange(Location location)
            +onProviderEnabled()
            +onProviderDisabled()
        }

        class ForegroundService {
            -Context applicationContext
            -StoreOnMetronomeTick storeOnMetronomeTick
            +onCreate()
            +onStartCommand(): Int
            +onDestroy()
        }
    }
    
    class NotificationCreator {
        -Context applicationContext
        -Resources resources
        -String CHANNEL_ID
        +NotificationCreator(Context applicationContext)
        +getNotification()
    }
    
    ForegroundService --o NotificationCreator

    class MetronomeListener {
        <<interface>>
        +onMetronomeTick(long duration)
    }

    class Metronome {
        -ScheduledExecutorService scheduler
        -ArrayList~MetronomeListener~ listeners
        +Metronome()
        +addListener(MetronomeListener listener)
        +removeListener(MetronomeListener listener)
        +informListener()
        +start()
        +stop()
        +getElapsedTime(): Long
    }

    namespace StoreCurrentTour {
        class StoreOnLocationChange {
            -TourRepository tourRepository
            -String speedLimitMoving
            -Location lastLocation
            +StoreOnLocationChange(TourRepository tourRepository, String speedLimitMoving)
            +onLocationChange(Location location)
        }
    
        class StoreCurrentTourLifeCycle {
            -TourRepository tourRepository
            +StoreCurrentTourLifeCycle(TourRepository tourRepository)
            +start()
            +pause()
            +createNewTour(): Tour
        }
    
        class StoreOnMetronomeTick {
            -TourRepository tourRepository
            -Metronome metronome
            -SpeedLimitsPojo speedLimits
            +StoreOnMetronomeTick(TourRepository tourRepository, SpeedLimitsPojo speedLimits)
            +start()
            +pause()
            +destroy()
            +onMetronomeTick(long duration)
            -getSpeedLimit(): double
            -getMovingType(double averageSpeedMoving): String
        }
    }
    
    class PermissionHandler {
        +isLocationPermissionGranted(Activity activity): boolean
        +isForegroundServicePermissionGranted(Activity activity): boolean
        +isWriteSettingsPermissionGranted(Context applicationContext): boolean
    }
    
    MainActivity --o PermissionHandler
    CurrentTourFragment --o PermissionHandler

    MainActivity --o CurrentTourFragment
    CurrentTourFragment --o ForegroundService
    ForegroundService --o LocationService

    LocationService --o StoreOnLocationChange
    StoreOnLocationChange --o TourRepository
    StoreCurrentTourLifeCycle --o TourRepository

    ForegroundService --o StoreOnMetronomeTick
    Metronome --o MetronomeListener
    StoreOnMetronomeTick --o Metronome
    StoreOnMetronomeTick --o TourRepository
    StoreOnMetronomeTick ..|> MetronomeListener

    CurrentTourVM --o TourRepository
    CurrentTourVM --o StoreCurrentTourLifeCycle

    CurrentTourFragment *-- CurrentTourVM
    
```
